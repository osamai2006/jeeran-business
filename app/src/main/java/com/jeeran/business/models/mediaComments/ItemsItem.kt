package com.jeeran.business.models.mediaComments

data class ItemsItem(val photoId: Int = 0,
                     val userId: Int = 0,
                     val created_at: Int = 0,
                     val id: Int = 0,
                     val user: com.jeeran.business.models.mediaComments.User,
                     val publishStatus: Int = 0,
                     val content: String = "")