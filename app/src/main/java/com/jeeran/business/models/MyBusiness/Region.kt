package com.jeeran.business.models.MyBusiness

data class Region(
    val city_id: Int,
    val id: Int,
    val name: String
)