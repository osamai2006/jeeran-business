package com.jeeran.business.models.mediaComments

data class User(val stats: com.jeeran.business.models.mediaComments.Stats,
                val photo_uri: String = "",
                val last_name: String = "",
                val id: Int = 0,
                val first_name: String = "",
                val username: String = "")