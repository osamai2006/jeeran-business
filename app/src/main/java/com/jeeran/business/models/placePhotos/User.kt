package com.jeeran.business.models.placePhotos

data class User(
    val first_name: String,
    val id: Int,
    val last_name: String,
    val photo_uri: PhotoUri,
    val stats: Stats,
    val username: String
)