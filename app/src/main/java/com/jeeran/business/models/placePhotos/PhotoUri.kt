package com.jeeran.business.models.placePhotos

data class PhotoUri(
    val large: String,
    val medium: String?,
    val thumb: String
)