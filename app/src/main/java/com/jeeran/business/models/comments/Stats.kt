package com.jeeran.business.models.comments

data class Stats(
    val reviews: Int
)