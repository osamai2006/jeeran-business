package com.jeeran.business.models.comments

data class CommentsModel(
    val items: List<Item>,
    val total_count: Int,
    val total_pages: Int
)