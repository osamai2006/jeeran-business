package com.jeeran.business.models.reviews

data class Stats(
    val comments: Int,
    val photos: Int,
    val positive_votes: Int,
    val total_votes: Int
)