package com.jeeran.business.models.reviews

data class City(
    val country_id: Int,
    val id: Int,
    val name: String,
    val ordering_enabled: Int,
    val search_term: String,
    val short_name: String
)