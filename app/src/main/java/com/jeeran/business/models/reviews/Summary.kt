package com.jeeran.business.models.reviews

data class Summary(
    val average: Double,
    val percentages: List<Double>,
    val stars: List<Int>,
    val total_reviews: Int
)