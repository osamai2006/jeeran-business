package com.jeeran.business.models.branches

data class Stats(val weeklyPhotosSd: Int = 0,
                 val twitterShares: Int = 0,
                 val weeklyCalls: Int = 0,
                 val averageRating: Int = 0,
                 val weeklyHeartsSd: Int = 0,
                 val weeklyPhotos: Int = 0,
                 val photos: Int = 0,
                 val shares: Int = 0,
                 val weeklyCallsSd: Int = 0,
                 val reviews: Int = 0,
                 val weeklyHearts: Int = 0,
                 val weeklyReviews: Int = 0,
                 val facebookShares: Int = 0,
                 val hearts: Int = 0,
                 val weeklyReviewsSd: Int = 0)