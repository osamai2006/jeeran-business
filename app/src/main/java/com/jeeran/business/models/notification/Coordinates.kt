package com.jeeran.business.models.notification

data class Coordinates(
    val lat: Double,
    val lon: Double
)