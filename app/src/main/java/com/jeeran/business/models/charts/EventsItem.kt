package com.jeeran.business.models.charts

data class EventsItem(val date: String = "",
                      val value: Int = 0)