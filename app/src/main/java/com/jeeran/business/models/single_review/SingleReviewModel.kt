package com.jeeran.business.models.single_review

data class SingleReviewModel(
    val city: City,
    val city_id: Int,
    val content: String,
    val context: Context,
    val created_at: Int,
    val featured: Int,
    val hearts: Int,
    val id: Int,
    val is_hearted: Boolean,
    val language: Int,
    val place: Place,
    val place_id: Int,
    val platform: String,
    val publish_status: Int,
    val rate: Int,
    val stats: Stats,
    val time_faded_order: Double,
    val updated_at: Int,
    val user: User,
    val user_id: Int
)