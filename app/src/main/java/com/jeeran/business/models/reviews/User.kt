package com.jeeran.business.models.reviews

data class User(
    val first_name: String,
    val id: Int,
    val last_name: String,
    val photo_uri: PhotoUri?,
    val stats: StatsX,
    val username: String
)