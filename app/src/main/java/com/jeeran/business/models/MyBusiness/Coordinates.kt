package com.jeeran.business.models.MyBusiness

data class Coordinates(
    val lat: Double,
    val lon: Double
)