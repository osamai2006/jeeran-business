package com.jeeran.business.models.comments

data class Item(
    val content: String,
    val created_at: Int,
    val id: Int,
    val language: Int,
    val parent_id: Any,
    val platform: String,
    val publish_status: Int,
    val review_id: Int,
    val user: User,
    val user_id: Int
)