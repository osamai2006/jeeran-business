package com.jeeran.business.models.chartFilters

data class FiltersItem(val name: String = "",
                       val short_name: String = "",
                       val id: Int = 0,
                       var isChecked: Int = 0)