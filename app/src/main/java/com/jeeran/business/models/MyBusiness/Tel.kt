package com.jeeran.business.models.MyBusiness

data class Tel(
    val id: Int,
    val phone_number: String,
    val place_id: Int
)