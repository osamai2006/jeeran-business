package com.jeeran.business.models.placePhotos

data class UriX(
    val large: String,
    val medium: String,
    val thumb: String
)