package com.jeeran.business.models.notification

data class Stats(
    val reviews: Int
)