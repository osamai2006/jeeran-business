package com.jeeran.business.models.MyBusiness

data class LogoUri(
    val large: String,
    val medium: String,
    val thumb: String
)