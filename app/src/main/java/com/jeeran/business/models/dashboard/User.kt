package com.jeeran.business.models.dashboard

data class User(
    val first_name: String?,
    val id: Int,
    val last_name: String?,
    val photo_uri: String?,
    val stats: Stats,
    val username: String
)