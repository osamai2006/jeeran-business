package com.jeeran.business.models.notification

data class PhotoUri(
    val large: String,
    val medium: String,
    val thumb: String
)