package com.jeeran.business.models.reviews

data class Place(
    val coordinates: Coordinates,
    val has_delivery: Boolean,
    val has_menu: Boolean,
    val has_online: Boolean,
    val id: Int,
    val is_premium: Boolean,
    val is_verified: Int,
    val logo_uri: LogoUri,
    val name: String,
    val open_now: Boolean,
    val ordering_link: String,
    val price_rating: Int,
    val short_name: String,
    val stats: Stats,
    val user_id: Int
)