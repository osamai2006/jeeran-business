package com.jeeran.business.models.dashboard

data class Stats(
    val reviews: Int
)