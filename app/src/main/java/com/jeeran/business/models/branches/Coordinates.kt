package com.jeeran.business.models.branches

data class Coordinates(val lon: Double = 0.0,
                       val lat: Double = 0.0)