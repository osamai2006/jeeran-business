package com.jeeran.business.models.single_review

data class City(
    val country_id: Int,
    val id: Int,
    val name_ar: String,
    val name_en: String,
    val ordering_enabled: Int,
    val search_term_ar: String,
    val search_term_en: String,
    val short_name: String
)