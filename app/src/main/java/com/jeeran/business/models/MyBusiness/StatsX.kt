package com.jeeran.business.models.MyBusiness

data class StatsX(
    val average_rating: Double,
    val facebook_shares: Int,
    val hearts: Int,
    val photos: Int,
    val reviews: Int,
    val shares: Int,
    val twitter_shares: Int
)