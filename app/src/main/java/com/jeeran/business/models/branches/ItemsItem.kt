package com.jeeran.business.models.branches

data class ItemsItem(val subscription_end_date: Int = 0,
                     val ownerNotify: Int = 0,
                     val userId: Int = 0,
                     val subscription_start_date: Int = 0,
                     val created_at: Int = 0,
                     val id: Int = 0,
                     val place: com.jeeran.business.models.branches.Place,
                     val acceptedAt: Int = 0,
                     val placeId: Int = 0,
                     val publishStatus: Int = 0)