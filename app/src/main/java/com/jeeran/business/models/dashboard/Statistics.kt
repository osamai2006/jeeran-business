package com.jeeran.business.models.dashboard

data class Statistics(
    val today: Int,
    val total: Int
)