package com.jeeran.business.models.single_review

data class PhotoUri(
    val large: String,
    val medium: String,
    val thumb: String
)