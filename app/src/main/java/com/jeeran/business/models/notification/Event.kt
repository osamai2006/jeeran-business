package com.jeeran.business.models.notification

data class Event(
    val event_id: Int,
    val id: Int,
    val is_read: Int,
    val message: String,
    val object_id: String,
    val place: Place,
    val place_id: String,
    val timestamp: Int,
    val user: User,
    val user_id: Int
)