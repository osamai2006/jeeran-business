package com.jeeran.business.models.countries

data class CitiesItem(val searchTerm: String = "",
                      val name: String = "",
                      val shortName: String = "",
                      val id: Int = 0,
                      val countryId: Int = 0,
                      val orderingEnabled: Int = 0)