package com.jeeran.business.models.reviews

data class Coordinates(
    val lat: Double,
    val lon: Double
)