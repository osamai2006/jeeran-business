package com.jeeran.business.models.reviews

data class ReviewsModel(
    val items: List<Item>,
    val summary: Summary,
    val total_count: Int,
    val total_pages: Int
)