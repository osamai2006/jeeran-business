package com.jeeran.business.models.countries

data class CountriesItem(val capitalId: Int = 0,
                         val languageCode: String = "",
                         val cities: List<CitiesItem>?,
                         val telephoneCode: String = "",
                         val name: String = "",
                         val currency: String = "",
                         val id: Int = 0,
                         val codeName: String = "")