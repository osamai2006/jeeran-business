package com.jeeran.business.models.features

data class ItemsItem(val name: String = "",
                     val id: Int = 0,
                     var selected: Boolean = false,
                     val items: List<ItemsItem>?)