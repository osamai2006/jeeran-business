package com.jeeran.business.models.dashboard

data class Uri(
    val large: String,
    val medium: String,
    val thumb: String
)