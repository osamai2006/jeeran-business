package com.jeeran.business.models.placePhotos

data class Stats(
    val reviews: Int,
    val likes: Int?
)