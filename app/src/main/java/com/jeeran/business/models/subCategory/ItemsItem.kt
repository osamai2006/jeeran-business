package com.jeeran.business.models.subCategory

data class ItemsItem(val name: String = "",
                     val shortName: String = "",
                     val id: Int = 0,
                     var selected: Boolean = false,
                     val type: Int = 0)