package com.jeeran.business.models.MyBusiness

data class PlaceCreator(
    val first_name: Any,
    val id: Int,
    val last_name: Any,
    val photo_uri: Any,
    val stats: Stats,
    val username: String
)