package com.jeeran.business.models.notification

data class NotificationModel(
    val events: List<Event>,
    val total_count: Int,
    val total_pages: Int
)