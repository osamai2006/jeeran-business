package com.jeeran.business.models.notification

data class NotificationCountModel(val notifications_count: Int = 0)