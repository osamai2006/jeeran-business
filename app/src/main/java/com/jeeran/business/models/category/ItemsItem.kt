package com.jeeran.business.models.category

data class ItemsItem(val places: Int = 0,
                     val reviews: Int = 0,
                     val name: String = "",
                     //val shortName: Null = null,
                     val id: Int = 0,
                     val type: Int = 0)