package com.jeeran.business.models.single_review

data class Coordinates(
    val lat: Double,
    val lon: Double
)