package com.jeeran.business.models.reviews

data class PhotoUri(
    val large: String,
    val medium: String,
    val thumb: String
)