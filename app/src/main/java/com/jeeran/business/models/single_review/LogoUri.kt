package com.jeeran.business.models.single_review

data class LogoUri(
    val large: String,
    val medium: String,
    val thumb: String
)