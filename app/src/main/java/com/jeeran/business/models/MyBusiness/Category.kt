package com.jeeran.business.models.MyBusiness

data class Category(
    val id: Int,
    val name: String,
    val short_name: String,
    val type: Int
)