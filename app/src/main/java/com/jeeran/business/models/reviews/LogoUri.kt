package com.jeeran.business.models.reviews

data class LogoUri(
    val large: String,
    val medium: String,
    val thumb: String
)