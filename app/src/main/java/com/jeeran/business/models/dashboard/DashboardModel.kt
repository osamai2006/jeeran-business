package com.jeeran.business.models.dashboard

data class DashboardModel(
    val latest_photos: List<LatestPhoto>,
    val reviews: Reviews,
    val statistics: Statistics
)