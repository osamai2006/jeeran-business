package com.jeeran.business.models.regions

data class ItemsItem(val name: String = "",
                     val id: Int = 0,
                     val cityId: Int = 0)