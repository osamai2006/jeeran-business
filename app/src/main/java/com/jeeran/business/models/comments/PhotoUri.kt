package com.jeeran.business.models.comments

data class PhotoUri(
    val large: String,
    val medium: String,
    val thumb: String
)