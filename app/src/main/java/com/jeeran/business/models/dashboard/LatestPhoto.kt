package com.jeeran.business.models.dashboard

data class LatestPhoto(
    val created_at: Int,
    val description: String?,
    val embed_code: Any,
    val featured: Int,
    val id: Int,
    val is_assigned_to_content: Int,
    val place_id: Int,
    val platform: Any,
    val product_id: Any,
    val publish_status: Int,
    val review_id: Any,
    val stat_comments: Int,
    val stat_facebook_shares: Int,
    val stat_likes: Int?,
    val stat_shares: Int,
    val stat_twitter_shares: Int,
    val uri: Uri,
    val uri_old: String,
    val user: User,
    val user_id: Int
)