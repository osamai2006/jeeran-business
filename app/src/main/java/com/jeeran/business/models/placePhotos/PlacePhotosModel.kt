package com.jeeran.business.models.placePhotos

data class PlacePhotosModel(
    val items: List<Item>,
    val premium_photo: PremiumPhoto,
    val total_count: Int,
    val total_pages: Int


)