package com.jeeran.business.models.branches

data class Region(val name: String = "",
                  val id: Int = 0,
                  val cityId: Int = 0)