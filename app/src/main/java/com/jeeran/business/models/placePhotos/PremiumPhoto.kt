package com.jeeran.business.models.placePhotos

data class PremiumPhoto(
    val created_at: Int,
    val description: String,
    val embed_code: Any,
    val featured: Int,
    val id: Int,
    val is_assigned_to_content: Int,
    val place_id: Int,
    val platform: Any,
    val product_id: Any,
    val publish_status: Int,
    val review_id: Any,
    val stat_comments: Int,
    val stat_facebook_shares: Int,
    val stat_likes: Int,
    val stat_shares: Int,
    val stat_twitter_shares: Int,
    var uri: Uri,
    val uri_old: Any,
    val user_id: Int
)