package com.jeeran.business.models.branches

data class Place(val brandCode: String = "",
                 val featured: Int = 0,
                 val directionsEn: String = "",
                 val street_ar: String = "",
                 val createdAt: Int = 0,
                 val directionsAr: String = "",
                 val street_en: String = "",
                 val adExpiresAt: Int = 0,
                 val userRole: Int = 0,
                 val updatedAt: Int = 0,
                 val stats: com.jeeran.business.models.branches.Stats,
                 val id: Int = 0,
                 val views: Int = 0,
                 val publishStatus: Int = 0,
                 val openingDate: Int = 0,
                 val venueId: String = "",
                 val primaryTagItemId: Int = 0,
                 val infoUpdatedAt: Int = 0,
                 val website: String = "",
                 val businessFeatured: Int = 0,
                 val regionId: Int = 0,
                 val coordinates: com.jeeran.business.models.branches.Coordinates,
                 val contentEn: String = "",
                 val contentAr: String = "",
                 val userId: Int = 0,
                 val name: String = "",
                 val business_email: String = "",
                 val shortName: String = "",
                 val streetId: String = "",
                 val region: com.jeeran.business.models.branches.Region,
                 val cityId: Int = 0)