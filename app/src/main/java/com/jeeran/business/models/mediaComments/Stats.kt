package com.jeeran.business.models.mediaComments

data class Stats(val reviews: Int = 0)