package com.jeeran.business.models.dashboard

data class Reviews(
    val total: Int,
    val unseen: Int
)