package com.jeeran.business.sms_services;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.jeeran.business.R;

public class RunReceiverSrvice extends Service {


    private CallBrodcastReceiver receiver = null;


    @Override
    public IBinder onBind(Intent intent)
    {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        try
        {
         super.onStartCommand(intent, flags, startId);



//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//            {
//
//                Intent notificationIntent = new Intent(getApplicationContext(), SplashActivity.class);
//                PendingIntent pendingIntent = PendingIntent.getActivity(this,
//                        0, notificationIntent, 0);
//
//                Notification notification = new NotificationCompat.Builder(this, "default")
//                        .setContentTitle("Example Service")
//                        .setContentText("bodydydydy")
//                        .setSmallIcon(R.drawable.icon)
//                        .setContentIntent(pendingIntent)
//                        .build();
//
//                startForeground(1, notification);;
//
//            } else {
//
//                NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                        .setContentTitle(getString(R.string.app_name))
//                        .setContentText(this.getString(R.string.app_name))
//                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                        .setAutoCancel(false);
//
//                Notification notification = builder.build();
//
//                startForeground(0, notification);
//            }


            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PHONE_STATE");
           // intentFilter.setPriority(100);
            registerReceiver(new CallBrodcastReceiver(), intentFilter);

            //Toast.makeText(getApplicationContext(), "Registered", Toast.LENGTH_LONG).show();
         }
         catch (Exception xx)
         {
             xx.toString();
         }
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        showNotification();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (receiver != null)
        {
            //unregisterReceiver(receiver);
           // Toast.makeText(getApplicationContext(), "Un Registered", Toast.LENGTH_LONG).show();
        }
    }


    private void showNotification()
    {
//        PendingIntent msgPendingIntent;
//        Intent msgsIntent = new Intent(this, SplashActivity.class);
//        msgPendingIntent = PendingIntent.getActivity(this, 0, msgsIntent,0);

        try {


            Notification notification = new NotificationCompat.Builder(this, "default")
                    .setContentTitle("Sending SMS Service")
                    .setContentText("This service allow Jeeran app sending SMS")
                    .setSmallIcon(R.drawable.icon)
                    .setContentIntent(null)
                    .setSound(null)
                    .build();

            startForeground(1, notification);
        }
        catch (Exception xx)
        {
            xx.toString();
        }

    }
}
