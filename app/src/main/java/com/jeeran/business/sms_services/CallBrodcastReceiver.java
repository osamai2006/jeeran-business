package com.jeeran.business.sms_services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class CallBrodcastReceiver extends BroadcastReceiver {

    private PhoneStateListener listener;
    private TelephonyManager telephonyManager;
    private boolean isPhoneCalling = false;

    @Override
    public void onReceive(final Context context, Intent intent)
    {
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
       listener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String phoneNumber)
            {

                switch (state)
                {
                    case TelephonyManager.CALL_STATE_OFFHOOK :

                        isPhoneCalling = true;
                        break;

                    case TelephonyManager.CALL_STATE_IDLE:

                        if (isPhoneCalling && !phoneNumber.equalsIgnoreCase(""))
                        {
                            try {

                                Uri sms_uri = Uri.parse("smsto:" + phoneNumber);
                                Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                                sms_intent.putExtra("sms_body", "Thank you our valued customer for the call, we will help you shortly");
                                context.startActivity(sms_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                                //Toast.makeText(context, "Sent to : " + phoneNumber, Toast.LENGTH_LONG).show();

                            } catch (Exception e) {
                                e.toString();
                                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                            }

                            isPhoneCalling = false;
                        }
                        break;

                }
            }
        };


        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);


    }

    }

