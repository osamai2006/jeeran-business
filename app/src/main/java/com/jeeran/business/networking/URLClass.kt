package com.jeeran.business.networking

class URLClass {

    companion object
    {
        val multiPartsURL ="https://jo.jeeran.com/api-5.0/call/PlacePhoto/uploadPhoto?place_id=";
        var BASEURL="https://api.jeeran.com/business/v1/"

//        val multiPartsURL ="https://stagingapi.jeeran.com/api-5.0/call/PlacePhoto/uploadPhoto?place_id="
//        var BASEURL="https://stagingapi.jeeran.com/business/v1/"

        var loginURL="login/"
        var registerURL="account/"
        var forgetPassURL="account/forgotpassword/"
        var userProfileURL="account/profile/"
        var updateProfileURL="account/"
        var reviewsURL="reviews/?type=byplace&id="
        var reviewSingleURL="reviews/"
        var commentsOfReviewURL="reviews/"
        var submitCommentURL="reviews/"
        var branchesURL="account/businesses/"
        var catURL="data/classes/"
        var subCatURL="data/classes/"
        var citiesURL="data/cities/"
        var regionURL="data/cities/"
        var featuresURL="data/features/"
        var businessDetailsURL="places/"
        var getNotificationsURL="account/notifications/?page="
        var getNotificationsCountURL="account/notifications/count/"
        var setNotificationsSetngURL="account/notifications/settings/"
        var getNotificationsSetngURL="account/notifications/settings/"
        var placePhotosURL="places/"
        var renewPlaceURL="request_renew/"
        var getChartFiltersURL="chart/filters/"
        var getStatisticsURL="chart/"
        var getDashBoardURL="account/stats/"
        var getSliderURL="slider/"
        var changePassURL="account/changepassword/"
        var editBusinessURL="request_business_edit/"
        var getPhotosCommentsURL="photos/"
        var addPhotosCommentsURL="photos/"
        var submitReviewAsReadURL="reviews/"
        var submitPremiumImageURL="photos/assignpremium/"
        var bindPhotoURL="photos/bind/"

        var aboutURL="about/"

    }


}