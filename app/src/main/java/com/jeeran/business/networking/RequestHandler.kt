package com.jeeran.business.networking

import android.app.Activity
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.loadingWheel
import com.jeeran.business.utils.sharedPrefs
import org.json.JSONObject
import java.lang.Exception
import java.util.HashMap

class RequestHandler{


    private var requestTimeOut=60000*2
    companion object{

        private lateinit var instance: com.jeeran.business.networking.RequestHandler
        var responseHandler: com.jeeran.business.networking.RequestHandler.ResponseHandler? = null
        fun getInstance(_responseHandler: com.jeeran.business.networking.RequestHandler.ResponseHandler): com.jeeran.business.networking.RequestHandler
        {
           RequestHandler.Companion.responseHandler = _responseHandler
           RequestHandler.Companion.instance = com.jeeran.business.networking.RequestHandler()
            return RequestHandler.Companion.instance

        }


    }

     fun requestAPI( requestMethod:String,requestBody: JSONObject?, endPointName: String, activity: Activity) {
        var context =activity.applicationContext
         var url= com.jeeran.business.networking.URLClass.Companion.BASEURL +endPointName
         var jsonObjReq: JsonObjectRequest
        loadingWheel.startSpinwheel(activity, false, false)

        var method= Request.Method.POST

         if(requestMethod.equals("post",ignoreCase = true))
         {
            method= Request.Method.POST
         }
         else  if(requestMethod.equals("get",ignoreCase = true))
         {
             method= Request.Method.GET
         }
         else  if(requestMethod.equals("put",ignoreCase = true))
         {
             method= Request.Method.PUT
         }
         else  if(requestMethod.equals("delete",ignoreCase = true))
         {
             method= Request.Method.DELETE
         }


        Thread(Runnable
        {
            try {

                jsonObjReq = object : JsonObjectRequest(method, url, requestBody, Response.Listener { response ->
                    try {
                        loadingWheel.stopLoading()
                        if (response != null)
                        {
                            if(ErrorHandler.Companion.getInstance().checkErrors(response))
                            {
                               RequestHandler.Companion.responseHandler?.successRequest(response, endPointName,requestMethod)
                            }
                        }

                    } catch (error: Exception) {
                        Toast.makeText(context, endPointName + " " + error.toString(), Toast.LENGTH_SHORT).show();
                        loadingWheel.stopLoading()
                        RequestHandler.Companion.responseHandler?.failedRequest("Server Error", endPointName)
                    }
                }, Response.ErrorListener { error ->
                    loadingWheel.stopLoading()
                    error.printStackTrace()
                    error.networkResponse
                   // ErrorHandler.Companion.getInstance().checkErrors("401")
                    Toast.makeText(context, "$endPointName $error", Toast.LENGTH_SHORT).show()
                    RequestHandler.Companion.responseHandler?.failedRequest("Server Error", endPointName)
                }) {
                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        val headersSys = super.getHeaders()
                        val headers = HashMap<String, String>()
                        headersSys.remove("Authorization")
                        headers["Authorization"] ="Token "+appConstants.userToken
                        headers["Accept-Language"] = appConstants.current_language

                        headers.putAll(headersSys)
                        return headers
                    }
                }

                var requestQueue = Volley.newRequestQueue(context)
                jsonObjReq.setRetryPolicy(DefaultRetryPolicy(
                        requestTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
                requestQueue.add(jsonObjReq)

            } catch (ex: Exception) {
                loadingWheel.stopLoading()
            }
        }).start()

    }


    interface ResponseHandler {

        fun successRequest(response: JSONObject, apiName: String, requestType: String)
        fun failedRequest(msg: String, apiName: String)
    }
}


