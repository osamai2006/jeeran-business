package com.jeeran.business.networking

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import org.json.JSONObject


class ErrorHandler {

    companion object{

        private lateinit var instance: ErrorHandler
        private lateinit var activity: Activity
        fun getInstance(): ErrorHandler
        {
            instance = ErrorHandler()
            this.activity=AppController.getInstance().getCurrentActivity()
            return instance

        }
    }


    fun checkErrors(response:JSONObject):Boolean
    {
        try
        {
            var errorCode=response.getJSONObject("response_status").getString("code")
            if(errorCode.equals("200",true))
            {
                var desc=response.getJSONObject("response_status").getString("internalMessage")
                if(!desc.equals("",true))
                {
                    Toast.makeText(com.jeeran.business.utils.AppController.getInstance().getCurrentActivity(), desc, Toast.LENGTH_SHORT).show();
                }
                return true

            }
            else   if(errorCode.equals("401",true))
            {
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userID_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userPhoto_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userName_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userEmail_KEY,"")
                sharedPrefs.getInstance().setBooleanPreference(activity!!.applicationContext, appConstants.isLoggedInKEY,false)
                appConstants.getDashboardCalled=false
                appConstants.getBusinessCalled=false
                appConstants.dahsboardAnimationFinished=false

                val i = activity!!.baseContext.packageManager.getLaunchIntentForPackage(activity!!.baseContext.packageName)
                i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                activity!!.startActivity(i)

                activity!!.finish()
            }
            else
            {
                var errorDesc=response.getJSONObject("response_status").getString("internalMessage")
                if(errorDesc.equals(""))
                {
                    errorDesc=response.getJSONObject("response_status").getString("message")
                }

                Toast.makeText(com.jeeran.business.utils.AppController.getInstance().getCurrentActivity(), errorDesc, Toast.LENGTH_SHORT).show();
                return false

            }

        }
        catch (xx:Exception)
        {
            //Toast.makeText(AppController.getInstance().getCurrentActivity(), xx.toString(), Toast.LENGTH_SHORT).show();
            return true
        }

        return true
    }


    fun checkErrors(errorCode:String):Boolean
    {
        try
        {
            if(errorCode.equals("401",true))
            {
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userID_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userPhoto_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userName_KEY,"")
                sharedPrefs.getInstance().setStringPreference(activity!!.applicationContext, appConstants.userEmail_KEY,"")
                sharedPrefs.getInstance().setBooleanPreference(activity!!.applicationContext, appConstants.isLoggedInKEY,false)
                appConstants.getDashboardCalled=false
                appConstants.getBusinessCalled=false
                appConstants.dahsboardAnimationFinished=false

                val i = activity!!.baseContext.packageManager.getLaunchIntentForPackage(activity!!.baseContext.packageName)
                i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                activity!!.startActivity(i)

                activity!!.finish()
            }

            return false
        }
        catch (xx:Exception)
        {
            //Toast.makeText(AppController.getInstance().getCurrentActivity(), xx.toString(), Toast.LENGTH_SHORT).show();
            return true
        }

        return true
    }
}