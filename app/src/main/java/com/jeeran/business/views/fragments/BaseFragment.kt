package com.jeeran.business.views.fragments

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment


import androidx.fragment.app.FragmentTransaction
import com.jeeran.business.R
import com.jeeran.business.views.activities.BaseActivity


abstract class BaseFragment : Fragment() {


    fun setScreenTitle(title: String) {
       BaseActivity.titleTXT.text = title
    }

    fun setNotificCount(count: String) {
        //titleTXT = findViewById(R.id.toolbar_title)
        BaseActivity.notiCountTXT.text = count
    }

    fun hideNavigationBarVisibility(visibility: Boolean,activity:Activity) {
        var navigationBarVisibility  = true
        if (visibility) {
            val decorView =activity!!.window.decorView
            val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            decorView.systemUiVisibility = uiOptions
            navigationBarVisibility = false
        } else
            navigationBarVisibility = true
    }

    fun replaceFragment(fragment: Fragment,addToStack:Boolean)
    {
        var tx: FragmentTransaction
        tx = activity!!.supportFragmentManager.beginTransaction()
        tx.replace(R.id.main_container, fragment)
        if(addToStack==true)
        {
            tx.addToBackStack(null)
        }
        tx.commit()
    }


}
