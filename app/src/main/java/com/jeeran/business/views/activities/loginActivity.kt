package com.jeeran.business.views.activities


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import org.json.JSONObject
import com.daimajia.slider.library.SliderLayout
import com.google.gson.Gson
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.jeeran.business.R
import com.jeeran.business.databinding.LoginActiviyBinding
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import java.lang.Exception


class loginActivity : BaseActivity(), View.OnClickListener, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        if(apiName.equals(URLClass.loginURL,true))
        {
            var id: String = response.getString("id")
            var photo: String = response.getJSONObject("photo_uri").getString("medium")
            var name: String = response.getString("display_name")
            var userToken: String = response.getString("bearer_token")

            sharedPrefs.getInstance().setStringPreference(this, appConstants.userID_KEY, id)
            sharedPrefs.getInstance().setStringPreference(this, appConstants.userToken_KEY, userToken)
            sharedPrefs.getInstance().setStringPreference(this, appConstants.userPhoto_KEY, photo)
            sharedPrefs.getInstance().setStringPreference(this, appConstants.userName_KEY, name)
            sharedPrefs.getInstance().setStringPreference(this, appConstants.userEmail_KEY, binding.emailTXT.text.toString())
            sharedPrefs.getInstance().setBooleanPreference(this, appConstants.isLoggedInKEY, true)

            startActivity(Intent(this@loginActivity, MainActivity::class.java))
            finish()
        }
        if(apiName.equals(URLClass.getSliderURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.slider.SliderModel::class.java)
            var sliderModel = modelObj as com.jeeran.business.models.slider.SliderModel

            for (i in 0..sliderModel.images!!.size-1 )
            {
                val textSliderView = DefaultSliderView(this)
                textSliderView.description("")
                        .image(sliderModel.images!!.get(i).image_url)
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        //.setOnSliderClickListener(this@loginActivity)
                textSliderView.bundle(Bundle())
                textSliderView.bundle.putString("extra", "")

                binding.sliderLayout.addSlider(textSliderView)
            }
            binding.sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default)
            binding.sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            binding.sliderLayout.setCustomAnimation(DescriptionAnimation())
            binding.sliderLayout.setDuration(3000)

        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }


    lateinit var binding: LoginActiviyBinding

    lateinit var bannerArraylist: ArrayList<com.jeeran.business.models.slider.SliderModel>
    var sliderModel: com.jeeran.business.models.slider.SliderModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        try {


        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding = DataBindingUtil.setContentView(this, R.layout.login_activiy)
        binding.loginBTN.setOnClickListener(this)
        binding.registerBTN.setOnClickListener(this)
        binding.forgetPassBTN.setOnClickListener(this)
        //binding.tutorialBTN.setOnClickListener(this)
        hideNavigationBarVisibility(true)
        getSlides()
    }
        catch (xx:Exception)
        {
            xx.toString()
        }

}

    override fun onResume() {
        super.onResume()

        binding.registerBTN.isClickable=true
        binding.registerBTN.isEnabled=true
    }
    override fun onClick(v: View?)
    {
        if (v?.id == R.id.loginBTN)
        {
            if(checkFields())
            {

                var body= JSONObject()
                body.putOpt("email",binding.emailTXT.text.toString().trim())
                body.putOpt("password",binding.passwordTXT.text.toString().trim())
                body.putOpt("fire_token",sharedPrefs.getInstance().getStringPreference(this!!,appConstants.mobileToken_KEY))

                com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.loginURL,this)
            }

        }

        if (v?.getId() == R.id.registerBTN)
        {
            binding.registerBTN.isClickable=false
            binding.registerBTN.isEnabled=false

            startActivity(Intent(this@loginActivity, RequestRegistrationActivity::class.java))
        }
        if (v?.getId() == R.id.forgetPassBTN)
        {
            startActivity(Intent(this@loginActivity, ForgetPassActivity::class.java))
        }


    }

    fun getSlides()
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.getSliderURL,this!!)

    }

    private fun checkFields(): Boolean {

        if (binding.emailTXT.text.toString().trim().equals("")) {
            Toast.makeText(applicationContext, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(binding.emailTXT.text.toString()).matches())
        {
            Toast.makeText(applicationContext, R.string.invalid_email, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.passwordTXT.text.toString().trim().equals("")) {
            Toast.makeText(applicationContext, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }
}
