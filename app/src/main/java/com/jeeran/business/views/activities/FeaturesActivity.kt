package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.models.countries.CountriesModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.views.adapters.CountryAdapter
import com.jeeran.business.views.adapters.CountryAdapterCallback
import org.json.JSONObject
import android.app.Activity
import android.content.Intent
import android.view.View
import com.jeeran.business.databinding.CountriesActivityBinding
import com.jeeran.business.databinding.FeatureActivityBinding
import com.jeeran.business.models.features.FeaturesModel
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.appConstants
import com.jeeran.business.views.adapters.FeaturesAdapterCallback
import com.jeeran.business.views.adapters.FeaturesMainAdapter
import org.json.JSONArray


class FeaturesActivity : AppCompatActivity(), RequestHandler.ResponseHandler, FeaturesAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, fetureID: Int, fetureName: String) {

        val resultIntent = Intent()
        resultIntent.putExtra("fetureID", fetureID)
        resultIntent.putExtra("fetureName", fetureName)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        return
    }


    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.equals(com.jeeran.business.networking.URLClass.featuresURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), FeaturesModel::class.java)
            featuresModel = modelObj as FeaturesModel


            var adapter = FeaturesMainAdapter(featuresModel,this)
            binding.featureLV.adapter=adapter
            binding.featureLV.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL ,false)

//            for (i in 0..binding.citySpin.adapter.getCount()-1)
//            {
//                if(businessModel.city.id==citiesModel.items!!.get(i).id)
//                {
//                    binding.citySpin.setSelection(i)
//                    break
//                }
//
//            }
        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }


    lateinit var binding: FeatureActivityBinding
    lateinit var featuresModel: FeaturesModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this, R.layout.feature_activity)

        getFeatures()

        binding.submitBTN.setOnClickListener(View.OnClickListener {

            var arr: JSONArray = JSONArray()
            var selectedFeatureNames:String=""

            for(i in 0..featuresModel.items!!.size-1)
            {
                for(j in 0..featuresModel.items!!.get(i).items!!.size-1)
                {
                    if(featuresModel.items!!.get(i).items!!.get(j).selected==true)
                    {
                        arr.put(featuresModel.items!!.get(i).items!!.get(j).id)
                        selectedFeatureNames=selectedFeatureNames+featuresModel.items!!.get(i).items!!.get(j).name+"  "
                    }
                }

            }
            val resultIntent = Intent()
            appConstants.selectedFeaturArr=arr
            resultIntent.putExtra("fetureNames",selectedFeatureNames)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()

        })

    }

    fun getFeatures()
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, URLClass.featuresURL,this)

    }
}
