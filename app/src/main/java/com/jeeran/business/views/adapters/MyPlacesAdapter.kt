package com.jeeran.business.views.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import com.jeeran.business.utils.appConstants
import kotlinx.android.synthetic.main.my_places_item_row.view.*
import kotlinx.android.synthetic.main.reviews_item_row.view.mainLO
import kotlinx.android.synthetic.main.reviews_item_row.view.nameTXT


class MyPlacesAdapter(private var _items : com.jeeran.business.models.branches.BranchModel, private var _callback:MyPlacesAdapterCallback) :
        RecyclerView.Adapter<MyPlacesViewHolder>()
{


    var  items : com.jeeran.business.models.branches.BranchModel
    var callback: MyPlacesAdapterCallback

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPlacesViewHolder
    {
        return MyPlacesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_places_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: MyPlacesViewHolder, position: Int)
    {
        holder?.nameTXT?.text = items.items!!.get(position).place.name
        holder?.addressTXT?.text = items.items!!.get(position).place.region.name
        holder?.sdateTXT?.text = appConstants.millisToDate(items.items!!.get(position).subscription_start_date.toLong())
        holder?.edateTXT?.text =appConstants.millisToDate(items.items!!.get(position).subscription_end_date.toLong())

        holder.mainLO.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.mainLO,position)
        })

        holder.renewBTN.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.renewBTN,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.items!!.size
    }

}

 class MyPlacesViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val nameTXT = view.nameTXT
    val sdateTXT = view.sdateTXT
    val edateTXT = view.edateTXT
    val addressTXT = view.addressTXT
    val renewBTN = view.renewBTN
    val mainLO = view.mainLO
}

 interface MyPlacesAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}