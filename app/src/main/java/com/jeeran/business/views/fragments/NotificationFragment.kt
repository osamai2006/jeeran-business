package com.jeeran.business.views.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.NotificationFragmentBinding
import com.jeeran.business.models.notification.NotificationModel
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.NotificationAdapter
import org.json.JSONObject
import android.widget.Toast
import androidx.core.view.ViewCompat.canScrollVertically
import androidx.recyclerview.widget.RecyclerView




class NotificationFragment : BaseFragment(), com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        if(apiName.equals(URLClass.getNotificationsURL+pageNum.toString(),true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.notification.NotificationModel::class.java)
            notificationModel = modelObj as com.jeeran.business.models.notification.NotificationModel

            totalPages=notificationModel.total_pages

            var adapter = NotificationAdapter(notificationModel)
            binding.newNotiLV.adapter = adapter
            binding.newNotiLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        }
    }

    override fun failedRequest(msg: String, apiName: String) {
      return
    }

    lateinit var binding: NotificationFragmentBinding
    lateinit var notificationModel: NotificationModel
    var pageNum=1
    var totalPages=0




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.notification_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.notificaation))

        getNotificatoins(pageNum)


       binding.newNotiLV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)


                if (!recyclerView.canScrollVertically(1))
                {
                    //Toast.makeText(activity, "Last", Toast.LENGTH_LONG).show()
                    if(pageNum<totalPages)
                    {
                        pageNum+=1
                        getNotificatoins(pageNum)

                    }

                }
//                else if (!recyclerView.canScrollVertically(-1))
//                {
//                    Toast.makeText(activity, "TOOOP", Toast.LENGTH_LONG).show()
////                    if(pageNum<totalPages)
////                    {
////                        pageNum+=1
////                        getNotificatoins(pageNum)
////
////                    }
//
//                }

            }

           override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
               super.onScrolled(recyclerView, dx, dy)
               if (dy < 0 && !recyclerView.canScrollVertically(-1))
               {
                   Toast.makeText(activity, "TOOOP", Toast.LENGTH_LONG).show()
                   if(pageNum<=totalPages)
                    {
                        pageNum-=1
                        getNotificatoins(pageNum)

                    }

               }
           }


        })

        return view
    }




    fun getNotificatoins(page:Int)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, URLClass.getNotificationsURL+page.toString(),activity as Activity)
    }

}
