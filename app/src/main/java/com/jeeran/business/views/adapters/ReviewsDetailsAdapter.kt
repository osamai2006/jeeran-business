package com.jeeran.business.views.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import com.jeeran.business.utils.appConstants
import kotlinx.android.synthetic.main.reviews_details_item_row.view.*


class ReviewsDetailsAdapter(private var _model : com.jeeran.business.models.comments.CommentsModel) :
        RecyclerView.Adapter<ReviewsDetailsViewHolder>()
{


    var  model : com.jeeran.business.models.comments.CommentsModel
    lateinit var context:Context
    init
    {
        model=_model

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewsDetailsViewHolder
    {
        context=parent.context
        return ReviewsDetailsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.reviews_details_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: ReviewsDetailsViewHolder, position: Int)
    {
        holder?.userNameTXT?.text = model.items!!.get(position).user.first_name+" "+ model.items!!.get(position).user.last_name
        holder?.dateTXT?.text = appConstants.millisToDate(model.items!!.get(position).created_at.toLong())
        holder?.commentTXT?.text = model.items!!.get(position).content

        Glide.with(context).load(model.items!!.get(position).user.photo_uri.medium)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                .into(holder.userIMG)
    }




    override fun getItemCount(): Int
    {
        return model.items!!.size
    }

}

class ReviewsDetailsViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val userNameTXT = view.userNameTXT
    val dateTXT = view.dateTXT
    val userIMG = view.userIMG
    val commentTXT = view.commentTXT
}