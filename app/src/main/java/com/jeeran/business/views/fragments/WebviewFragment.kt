package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.WebviewrFragmentBinding
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.loadingWheel


class WebviewFragment : BaseFragment()
{

    lateinit var binding: WebviewrFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.webviewr_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root


        setScreenTitle(getString(R.string.edit_my_business))
        binding.webView.clearCache(true)
        binding.webView.clearHistory()
        binding.webView.getSettings().setJavaScriptEnabled(true)
        binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)

        binding.webView.setWebViewClient(object : WebViewClient()
        {


            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
                super.onPageStarted(view, url, favicon)
                loadingWheel.startSpinwheel(context!!, false, true)

            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                loadingWheel.stopLoading()
            }
        })

        try
        {
            binding.webView.loadUrl(appConstants.webviewURL)
        }
        catch ( xx:Exception)
        {
            xx.toString()
        }


        return view
    }



}
