package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.networking.RequestHandler
import org.json.JSONObject
import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.ArrayAdapter
import com.jeeran.business.databinding.SubcategoriesActivityBinding
import com.jeeran.business.models.category.CategoryModel
import com.jeeran.business.models.subCategory.SubCategoryModel
import com.jeeran.business.utils.appConstants
import com.jeeran.business.views.adapters.CategoryAdapter
import com.jeeran.business.views.adapters.CategoryAdapterCallback
import com.jeeran.business.views.fragments.EditPlaceFragment
import org.json.JSONArray


class SubCategoriesActivity : AppCompatActivity(), RequestHandler.ResponseHandler, CategoryAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, position: Int) {

       return
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        return
    }


    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.contains("subcategories/",true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), SubCategoryModel::class.java)
            subCatModel = modelObj as SubCategoryModel


            var adapter = CategoryAdapter(subCatModel,this)
            binding.subCatLV.adapter=adapter
            binding.subCatLV.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL ,false)



        }

    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }


    lateinit var binding: SubcategoriesActivityBinding
    lateinit var subCatModel: SubCategoryModel


    companion object{
        var catID=""
    }
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this, R.layout.subcategories_activity)

        getSubCategory(catID)

        binding.submitBTN.setOnClickListener(View.OnClickListener {

            var arr:JSONArray=JSONArray()
            var SelectedSubCatNames:String=""

            for(i in 0..subCatModel.items!!.size-1)
            {
                if(subCatModel.items!!.get(i).selected==true)
                {
                    arr.put(subCatModel.items!!.get(i).id)
                    SelectedSubCatNames=SelectedSubCatNames+subCatModel.items!!.get(i).name+"  "
                }
            }
            val resultIntent = Intent()
            appConstants.selectedSubCatArr=arr
            resultIntent.putExtra("subCatNames",SelectedSubCatNames)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()

        })

    }

    fun getSubCategory(catId:String)
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.subCatURL+catId+"/subcategories/",this)

    }
}
