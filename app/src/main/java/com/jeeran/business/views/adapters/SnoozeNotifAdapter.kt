package com.jeeran.business.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import kotlinx.android.synthetic.main.snooze_item_row.view.*

class SnoozeNotifAdapter(private var _items : MutableList<String>,private var _callback:snoozeAdapterCallback) :
        RecyclerView.Adapter<snoozeAdapterView>()
{
     var  items : MutableList<String>
     var callback: snoozeAdapterCallback

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): snoozeAdapterView
    {
        return snoozeAdapterView(LayoutInflater.from(parent.context).inflate(R.layout.snooze_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: snoozeAdapterView, position: Int)
    {
        holder.descTXT.text = items.get(position).toString()

        holder.mainLO.setOnClickListener(View.OnClickListener
        {
             callback.onRecyclItemClicked(R.id.mainLO,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.size
    }

}

class snoozeAdapterView (view: View) : RecyclerView.ViewHolder(view)
{
    val descTXT = view.descTXT
    val mainLO = view.mainLO

}


interface snoozeAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}