package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.ReviewsDetailFragmentBinding
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.ReviewsDetailsAdapter
import org.json.JSONObject
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.jeeran.business.models.single_review.SingleReviewModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass


class ReviewsDetailsFragment : BaseFragment(), com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        var userID=sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY)

        if(apiName.equals(URLClass.reviewSingleURL+appConstants.reviewID+"/?user_id="+userID,true) && requestType.equals("get",true)) {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), SingleReviewModel::class.java)
            singleReviewModel = modelObj as SingleReviewModel

            binding.nameTXT.text = singleReviewModel.user.first_name+" "+singleReviewModel.user.last_name
            binding.ratingBar.rating=singleReviewModel.rate.toFloat()
            binding.dateTXT.text = appConstants.millisToDate(singleReviewModel.created_at.toLong())
            binding.reviewTXT.text = singleReviewModel.content
            binding.heartsCountTXT.text = singleReviewModel.hearts.toString()


            Glide.with(context).load(singleReviewModel.user.photo_uri.medium)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(binding.userImg)
        }
        else if(apiName.equals(com.jeeran.business.networking.URLClass.submitCommentURL+appConstants.reviewID+"/comments/")&& requestType.equals("post",true))
        {
            //Toast.makeText(AppController.getInstance().getCurrentActivity(), "Success", Toast.LENGTH_SHORT).show();
            getComments(appConstants.reviewID)
        }
        else if(apiName.equals(com.jeeran.business.networking.URLClass.commentsOfReviewURL+appConstants.reviewID+"/comments/"))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.comments.CommentsModel::class.java)
            commentsModel = modelObj as com.jeeran.business.models.comments.CommentsModel

            var adapter:ReviewsDetailsAdapter = ReviewsDetailsAdapter(commentsModel)
            binding.commentsLV.adapter=adapter
            binding.commentsLV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL ,false)

            binding.commentsCountTXT.setText(commentsModel.items!!.size.toString())

           // (binding.commentsLV.layoutManager as LinearLayoutManager).setReverseLayout(true);

//            val scrollTo = (binding.commentsLV.getParent().getParent() as View).bottom
//            binding.mainScroll.smoothScrollTo(0, scrollTo)
//
//            val siz = commentsModel.items!!.size
//            binding.mainScroll.fullScroll(View.FOCUS_DOWN);
//            (binding.commentsLV.layoutManager as LinearLayoutManager).scrollToPosition(adapter.itemCount-1)
           // binding.commentsLV.smoothScrollToPosition((binding.commentsLV.adapter as ReviewsDetailsAdapter).itemCount-1)


        }


    }

    override fun failedRequest(msg: String, apiName: String) {
       return
    }

    lateinit var binding: ReviewsDetailFragmentBinding
    lateinit var singleReviewModel: SingleReviewModel
    lateinit var commentsModel: com.jeeran.business.models.comments.CommentsModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.reviews_detail_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.reviws))

        getSingleReview(appConstants.reviewID)
        getComments(appConstants.reviewID)

        binding.sendBTN.setOnClickListener(View.OnClickListener {

            if(!binding.commentToSubmitTXT.text.toString().equals("",true))
            {
              submitComments(appConstants.reviewID,binding.commentToSubmitTXT.text.toString())
                binding.commentToSubmitTXT.setText("")
            }
        })

        submitReviewAsRead(appConstants.reviewID)
        return view
    }

    fun getSingleReview(reviewID:Int)
    {
        var userID=sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY)
        RequestHandler.getInstance(this).requestAPI("get", null, URLClass.reviewSingleURL+reviewID+"/?user_id="+userID,activity!!)

    }

    fun getComments(reviewID:Int)
    {
        RequestHandler.getInstance(this).requestAPI("get", null, URLClass.commentsOfReviewURL+reviewID+"/comments/",activity!!)

    }

    fun submitComments(reviewID:Int,comment:String)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY))
        body.putOpt("content",comment)
        body.putOpt("platform","Android App")

       RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.submitCommentURL+reviewID+"/comments/",activity!!)

    }

    fun submitReviewAsRead(reviewID:Int)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY))
        body.putOpt("review_id",reviewID)

        RequestHandler.getInstance(this).requestAPI("put", body, com.jeeran.business.networking.URLClass.submitReviewAsReadURL+reviewID+"/read/",activity!!)

    }

}
