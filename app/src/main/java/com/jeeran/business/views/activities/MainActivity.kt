package com.jeeran.business.views.activities

import android.Manifest
import android.app.AlertDialog

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.nav_header.view.*
import org.json.JSONObject
import android.os.Build
import android.util.Log
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.jeeran.business.R
import com.jeeran.business.databinding.MainActivityBinding
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.fragments.DashboardFragment
import com.jeeran.business.views.fragments.NotificationFragment
import com.jeeran.business.views.fragments.NotificationSettingFragment
import com.jeeran.business.views.fragments.ProfileFragment
import kotlin.math.log


class MainActivity : BaseActivity(), com.jeeran.business.networking.RequestHandler.ResponseHandler,NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{


    override fun onClick(v: View?)
    {
        if(v!!.id==R.id.notificationBTN)
        {
            mainActivitbinding.drawerLayout.closeDrawer(GravityCompat.START);
            replaceFragment(NotificationFragment(),true)

        }

        if(v!!.id==R.id.profileIMG)
        {
            mainActivitbinding.drawerLayout.closeDrawer(GravityCompat.START);
            replaceFragment(ProfileFragment(),true)
        }


    }

    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {


    }

    override fun failedRequest(msg: String, apiName: String)
    {
        return
    }

    lateinit var mainActivitbinding: MainActivityBinding

    lateinit var profileIMG:ImageView
    lateinit var profileNameTXT:TextView
    lateinit var profileEmailTXT:TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        AppController.getInstance().setCurrentActivity(this)
        setSupportActionBar(toolbar)
        mainActivitbinding=DataBindingUtil.setContentView(this, R.layout.main_activity)

        grantPermissions()

        hideNavigationBarVisibility(true)
        init()

    }


    private fun init()
    {
        try {

            mainActivitbinding.navView.inflateMenu(R.menu.menu_base)
            mainActivitbinding.navView.inflateHeaderView(R.layout.nav_header)
            mainActivitbinding.navView.setNavigationItemSelectedListener(this);

            val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()

            var header: View = mainActivitbinding.navView.getHeaderView(0);
            profileIMG = header.profileIMG
            profileEmailTXT = header.profileEmailTXT
            profileNameTXT = header.profileNameTXT

            profileNameTXT.setText(sharedPrefs.getInstance().getStringPreference(this, appConstants.userName_KEY))
            profileEmailTXT.setText(sharedPrefs.getInstance().getStringPreference(this, appConstants.userEmail_KEY))

            Glide.with(this).load(sharedPrefs.getInstance().getStringPreference(this, appConstants.userPhoto_KEY))
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.no_image_ic))
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(profileIMG)

            profileIMG.setOnClickListener(this)
            mainActivitbinding.notificationBTN.setOnClickListener(this)


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//        {
//            val serviceIntent = Intent(this, com.jeeran.business.sms_services.RunReceiverSrvice::class.java)
//            ContextCompat.startForegroundService(this,serviceIntent)
//
//        } else
//        {
//            startService(Intent(this, com.jeeran.business.sms_services.RunReceiverSrvice::class.java))
//        }

            replaceFragment(DashboardFragment(), false)
            setScreenTitle(getString(R.string.home_dr))
            setNotificationCount("0")
        }
        catch (xx:java.lang.Exception)
        {
            Log.d("init screen", xx.toString())
        }

    }


    fun grantPermissions() {

        try
        {
            if (ContextCompat.checkSelfPermission(applicationContext,
                            Manifest.permission.READ_EXTERNAL_STORAGE) === PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                            === PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            === PackageManager.PERMISSION_GRANTED   &&  (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
                            === PackageManager.PERMISSION_GRANTED) &&  (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                            === PackageManager.PERMISSION_GRANTED))
            {

            } else {

//                ActivityCompat.requestPermissions(this,
//                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE,
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS,
//                                Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE), 101)

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION), 101)


            }

        } catch (xx: Exception) {
            xx.message
        }


    }

    override fun onBackPressed() {
        try {

            if (mainActivitbinding.drawerLayout.isDrawerOpen(GravityCompat.START))
                {
                    mainActivitbinding.drawerLayout.closeDrawer(GravityCompat.START)
                }
           else
            {
                val fragments = supportFragmentManager.backStackEntryCount
                // List<Fragment> nameFragments=
                if (fragments == 0)
                {
                    AlertDialog.Builder(this)
                            .setIcon(R.mipmap.icon)
                            .setMessage(R.string.exit_msg)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, id ->

                                appConstants.getDashboardCalled=false
                                appConstants.getBusinessCalled=false
                                appConstants.dahsboardAnimationFinished=false

                                this@MainActivity.finish()
                            })
                            .setNegativeButton(R.string.no, null)
                            .show()
                } else {

                    super.onBackPressed()
                }
            }

        } catch (xx: Exception) {
        }

    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean
    {


        when(p0.itemId)
        {
            R.id.home_dr->
            {
                appConstants.getDashboardCalled=false
                appConstants.dahsboardAnimationFinished=false
                replaceFragment(DashboardFragment(),true)

            }

            R.id.profile_dr->
            {
                replaceFragment(ProfileFragment(),true)

            }

            R.id.notifi_seting_dr->
            {
                replaceFragment(NotificationSettingFragment(),true)

            }

            R.id.snooze_notifi_dr->
            {
                startActivity(Intent(this@MainActivity,SnoozenActivity::class.java))

            }

            R.id.lang_dr->
            {
                startActivity(Intent(this@MainActivity,LanguageActivity::class.java))
            }

            R.id.logout_dr->
            {
                Thread(Runnable {
                    try
                    {
                        FirebaseInstanceId.getInstance().deleteInstanceId()
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }).start()

                sharedPrefs.getInstance().setStringPreference(this,appConstants.userID_KEY,"")
                sharedPrefs.getInstance().setStringPreference(this,appConstants.userPhoto_KEY,"")
                sharedPrefs.getInstance().setStringPreference(this,appConstants.userName_KEY,"")
                sharedPrefs.getInstance().setStringPreference(this,appConstants.userEmail_KEY,"")
                sharedPrefs.getInstance().setBooleanPreference(this,appConstants.isLoggedInKEY,false)
                appConstants.getDashboardCalled=false
                appConstants.getBusinessCalled=false
                appConstants.dahsboardAnimationFinished=false

                val i = baseContext.packageManager.getLaunchIntentForPackage(baseContext.packageName)
                i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)

                finish()
            }
        }
        mainActivitbinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true

    }




}
