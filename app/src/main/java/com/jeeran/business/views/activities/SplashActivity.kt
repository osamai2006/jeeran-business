package com.jeeran.business.views.activities


import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Window
import android.view.WindowManager
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.SplashActivityBinding
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import java.util.*

class SplashActivity : BaseActivity() {

   lateinit  var binding: SplashActivityBinding

    lateinit var timer:CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        hideNavigationBarVisibility(true)



        if (Build.VERSION.SDK_INT >= 19)
        {
            getWindow().setFlags(AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT, AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT);
            getWindow().getDecorView().setSystemUiVisibility(3328);
        }else{
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        AppController.getInstance().setCurrentActivity(this)
        binding=DataBindingUtil.setContentView(this, R.layout.splash_activity)

        initLanguage()

      timer=object :CountDownTimer(3000,100)
      {
          override fun onTick(millisUntilFinished: Long)
          {
          }

          override fun onFinish()
          {
              val isLogged = sharedPrefs.getInstance().getBooleanPreference(applicationContext, appConstants.isLoggedInKEY, false)

              if (isLogged)
              {
                  val intent = Intent(this@SplashActivity, MainActivity::class.java)
                  startActivity(intent)
                  finish()

              }
              else
              {
                  val intent = Intent(this@SplashActivity, loginActivity::class.java)
                  startActivity(intent)
                  finish()
              }
          }
      }.start()



    }

    fun initLanguage()
    {
        var loc = "en"
        val currentLoc = sharedPrefs.getInstance().getStringPreference(getApplicationContext(), appConstants.language_KEY)
        if (currentLoc !== "") {
            loc = currentLoc
        } else {
            val current = getResources().getConfiguration().locale
            loc = current.getLanguage()
        }

        if (loc.equals("en", ignoreCase = true)) {
            appConstants.current_language = "en"
            val locale = Locale("en")
            val res = getResources()
            val dm = res.getDisplayMetrics()
            val conf = res.getConfiguration()
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            onConfigurationChanged(conf)

        } else {
            appConstants.current_language = "ar"
            val locale = Locale("ar")
            val res = getResources()
            val dm = res.getDisplayMetrics()
            val conf = res.getConfiguration()
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            onConfigurationChanged(conf)

        }
    }
}
