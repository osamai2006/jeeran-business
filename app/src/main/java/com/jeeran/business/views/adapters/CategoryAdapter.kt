package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import com.jeeran.business.models.category.CategoryModel
import com.jeeran.business.models.subCategory.SubCategoryModel
import kotlinx.android.synthetic.main.subcategory_item_row.view.*


class CategoryAdapter(private var _items : SubCategoryModel, private var _callback:CategoryAdapterCallback) :
        RecyclerView.Adapter<CategoryHolder>()
{


    var  items : SubCategoryModel
    var callback: CategoryAdapterCallback
    lateinit var  context: Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder
    {
        context=parent.context
        return CategoryHolder(LayoutInflater.from(parent.context).inflate(R.layout.subcategory_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: CategoryHolder, position: Int)
    {



        holder.catNameView.setText(items.items!!.get(position).name)

        holder.selectedCatIMG.setOnClickListener(View.OnClickListener {

            if(items.items!!.get(position).selected==true)
            {
                items.items!!.get(position).selected=false
                holder.selectedCatIMG.setImageResource(R.drawable.unselect_ic)

            }
            else
            {
                items.items!!.get(position).selected=true
                holder.selectedCatIMG.setImageResource(R.drawable.selected_ic)
            }

            callback.onRecyclItemClicked(R.id.selectedCatIMG,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.items!!.size
    }

}

 class CategoryHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val selectedCatIMG = view.selectedCatIMG
    val catNameView = view.catNameView


}

 interface CategoryAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}