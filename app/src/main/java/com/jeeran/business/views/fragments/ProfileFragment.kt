package com.jeeran.business.views.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.content.Intent
import android.provider.MediaStore
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import com.jeeran.business.databinding.ProfileFragmentBinding
import com.jeeran.business.utils.*
import com.jeeran.business.views.activities.ChangePassActivity
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.URL


class ProfileFragment : BaseFragment(), View.OnClickListener, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        if(apiName.equals(com.jeeran.business.networking.URLClass.userProfileURL,true))
        {
            var img=response.getJSONObject("photo_uri").getString("medium")
            var phone=response.getString("mobile")
            var position=response.getString("position")
            var email=response.getString("email")
            var name=response.getString("first_name")+" "+response.getString("last_name")
            var nickName=response.getString("display_name")

            Glide.with(context).load(img)
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.no_image_ic))
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(binding.userProfileIMG)

            binding.nameTXT.setText(name)
            binding.profEmailTXT.setText(email)
            binding.fullNameTXT.setText(name)
            binding.displayNameTXT.setText(nickName)
            binding.emailTXT.setText(email)
            binding.phoneTXT.setText(phone)
            binding.jobNameTXT.setText(position)
        }

        else  if(apiName.equals(com.jeeran.business.networking.URLClass.updateProfileURL,true))
        {
            this.fragmentManager!!.popBackStack()
        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    override fun onClick(v: View?) {
        if(v!!.id==R.id.changePassBTN)
        {
            startActivity(Intent(activity, ChangePassActivity::class.java))
        }

        if(v!!.id==R.id.submitBTN)
        {
            if(checkFields())
            {
                updateProfile()
            }
        }
        if(v!!.id==R.id.editImgBTN)
        {
            //selectImageInAlbum()

        }
    }

    lateinit var binding: ProfileFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.profile_dr))

        binding.changePassBTN.setOnClickListener(this)
        binding.submitBTN.setOnClickListener(this)
        binding.editImgBTN.setOnClickListener(this)

        getProfile()
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == 111){

            if (data != null)
            {
                loadingWheel.startSpinwheel(activity!!, false, false)
                Thread(Runnable {

                    var contentURI = data!!.data
                    try
                    {



                    }
                    catch (e: IOException) {
                        loadingWheel.stopLoading()
                        e.printStackTrace()
                        Toast.makeText(context, "Check your permissions !", Toast.LENGTH_SHORT).show()
                    }

                }).start()


            }
        }
    }

    fun getProfile()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.userProfileURL,activity as Activity)
    }

    fun updateProfile()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        body.putOpt("display_name", binding.displayNameTXT.text.toString())
        body.putOpt("position",binding.jobNameTXT.text.toString())
        body.putOpt("email",binding.emailTXT.text.toString())
        body.putOpt("password", "")
        body.putOpt("phone_number", binding.phoneTXT.text.toString())


        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("put", body, com.jeeran.business.networking.URLClass.updateProfileURL,activity as Activity)
    }

    private fun checkFields(): Boolean {

//        if (binding.fullNameTXT.text.toString().trim().equals("")) {
//            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
//            return false
//        }

        if (binding.displayNameTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.jobNameTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.emailTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.phoneTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    fun selectImageInAlbum()
    {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, 111)

    }
    fun takePhoto()
    {
        val intent1 = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent1.resolveActivity(AppController.getInstance().packageManager) != null) {
            startActivityForResult(intent1, 222)
        }
    }
}
