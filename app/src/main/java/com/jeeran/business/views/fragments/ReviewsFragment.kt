package com.jeeran.business.views.fragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.ReviewsFragmentBinding
import com.jeeran.business.models.reviews.ReviewsModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.ReviewsAdapter
import com.jeeran.business.views.adapters.ReviewsAdapterCallback
import org.json.JSONObject
import java.lang.Exception


class ReviewsFragment : BaseFragment(), ReviewsAdapterCallback, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.equals(URLClass.branchesURL,true))
        {

            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.branches.BranchModel::class.java)
            branchesModel = modelObj as com.jeeran.business.models.branches.BranchModel


            var spinnerItems :MutableList<String> = ArrayList()
            for (i in 0..branchesModel.items!!.size-1)
            {
                spinnerItems.add(branchesModel.items!!.get(i).place.name+" - "+branchesModel.items!!.get(i).place.region.name)
            }
            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.branchesSpin!!.setAdapter(adapter)
        }


        if(apiName.equals(URLClass.reviewsURL+branchID,true))
        {
            try {


                val gson = Gson()
                val modelObj = gson.fromJson<Any>(response.toString(), ReviewsModel::class.java)
                reviewModel = modelObj as ReviewsModel

                var adapter = ReviewsAdapter(reviewModel, this)
                binding.reviewsLV.adapter = adapter
                binding.reviewsLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                (binding.reviewsLV.layoutManager as LinearLayoutManager).setReverseLayout(true);


                binding.totalReviewTXT.text = reviewModel.summary.total_reviews.toString()
                binding.avgCircle.progress=reviewModel.summary.average.toInt()
                binding.avgCircleTXT.text = reviewModel.summary.average.toString()
                binding.ratingBar.rating = reviewModel.summary.average.toFloat()


                binding.star1PB.progress = reviewModel.summary.percentages!!.get(0).toInt()
                binding.star2PB.progress = reviewModel.summary.percentages!!.get(1).toInt()
                binding.star3PB.progress = reviewModel.summary.percentages!!.get(2).toInt()
                binding.star4PB.progress = reviewModel.summary.percentages!!.get(3).toInt()
                binding.star5PB.progress = reviewModel.summary.percentages!!.get(4).toInt()


                binding.start1PerctgTXT.text = reviewModel.summary.percentages!!.get(0).toString()+"%"
                binding.start2PerctgTXT.text = reviewModel.summary.percentages!!.get(1).toString()+"%"
                binding.start3PerctgTXT.text = reviewModel.summary.percentages!!.get(2).toString()+"%"
                binding.start4PerctgTXT.text = reviewModel.summary.percentages!!.get(3).toString()+"%"
                binding.start5PerctgTXT.text = reviewModel.summary.percentages!!.get(4).toString()+"%"

               // binding.reviewsLV.scrollToPosition(reviewModel.items!!.size-1)
            }
            catch (xx:Exception)
            {
                xx.toString()
            }


        }
    }

    override fun failedRequest(msg: String, apiName: String) {
           return
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        return
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int)
    {
        appConstants.reviewID = reviewModel.items!!.get(position).id
        replaceFragment(ReviewsDetailsFragment(),true)
    }



    lateinit var binding: ReviewsFragmentBinding
    lateinit var reviewModel: com.jeeran.business.models.reviews.ReviewsModel
    lateinit var branchesModel: com.jeeran.business.models.branches.BranchModel

    var branchID=""




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.reviews_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.reviws))

        getBranches()


        binding.branchesSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?)
            {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                try {
                    branchID=branchesModel.items!!.get(position).place.id.toString()
                    getReviews(branchID)
                }
                catch (xx:Exception)
                {
                    xx.toString()
                }

            }

        }

        return view
    }

    fun getBranches()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY))
        RequestHandler.getInstance(this).requestAPI("post", body, URLClass.branchesURL,activity!!)

    }

    fun getReviews(branchID:String)
    {
        RequestHandler.getInstance(this).requestAPI("get", null, URLClass.reviewsURL+branchID,activity!!)

    }


}
