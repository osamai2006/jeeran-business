package com.jeeran.business.views.activities


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.CustomDateDialogBinding
import com.jeeran.business.utils.AppController
import java.text.SimpleDateFormat
import java.util.*

class CustomDateActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        if (v?.id == R.id.submitBTN)
        {
        val format = SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH)
        val selectedDatesCalender = binding.calendarView.selectedDates
        if (selectedDatesCalender.size > 0) {
            startDate = format.format(selectedDatesCalender[0].time).toString().trim()
            endDate = format.format(selectedDatesCalender[selectedDatesCalender.size - 1].time).toString().trim()
        }
        if (endDate.equals("", ignoreCase = true))
        {
            endDate = startDate
        }
            val data = Intent()
            data.putExtra("sDate", startDate)
            data.putExtra("eDate", endDate)
            setResult(RESULT_OK, data)

            finish()
        }


    }

    lateinit var binding: CustomDateDialogBinding
    internal var startDate = "";
    internal var endDate = ""
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding= DataBindingUtil.setContentView(this, R.layout.custom_date_dialog)

        initCalender()
        binding.submitBTN.setOnClickListener(this)
    }

    fun initCalender()
    {
        val min = Calendar.getInstance()
        val max = Calendar.getInstance()
        min.add(Calendar.MONTH, -3)

        binding.calendarView.setMinimumDate(min)
        binding.calendarView.setMaximumDate(max)


    }
}
