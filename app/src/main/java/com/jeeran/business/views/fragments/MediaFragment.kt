package com.jeeran.business.views.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.content.Intent
import android.provider.MediaStore
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.MediaFragmentBinding
import com.jeeran.business.models.placePhotos.PlacePhotosModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.*
import com.jeeran.business.views.activities.AddPhotoDescActivity
import com.jeeran.business.views.adapters.MediaAdapter
import com.jeeran.business.views.adapters.MediaAdapterCallback
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception


class MediaFragment : BaseFragment(), MediaAdapterCallback, RequestHandler.ResponseHandler,UploadMultiPart.UploadeResponse {
    override fun uploadFinish(result: String)
    {
        loadingWheel.stopLoading()
        if(!result.toString().equals(""))
        {
           AddPhotoDescActivity.photoID=result
            AddPhotoDescActivity.placeID=branchID

            startActivity(Intent(activity!!,AddPhotoDescActivity::class.java))

        }

    }

    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.equals(URLClass.placePhotosURL+branchID+"/photos/",true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.placePhotos.PlacePhotosModel::class.java)
            placePhotosModel = modelObj as com.jeeran.business.models.placePhotos.PlacePhotosModel

            adapter = MediaAdapter(placePhotosModel,this)
            binding.mediaLV.adapter=adapter
       // binding.mediaLV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL ,false)
           binding.mediaLV.layoutManager = GridLayoutManager(context,3)

            Glide.with(context).load(placePhotosModel.premium_photo.uri.medium.toString())
                    //.apply(RequestOptions.circleCropTransform())
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(binding.headerIMG);

            setPremiumChecked()

        }

        if(apiName.equals(URLClass.branchesURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.branches.BranchModel::class.java)
            branchesModel = modelObj as com.jeeran.business.models.branches.BranchModel


            var spinnerItems :MutableList<String> = ArrayList()
            for (i in 0..branchesModel.items!!.size-1)
            {
                spinnerItems.add(branchesModel.items!!.get(i).place.name+" - "+branchesModel.items!!.get(i).place.region.name)
            }
            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.branchSpin!!.setAdapter(adapter)
        }

        if(apiName.equals(URLClass.submitPremiumImageURL,true))
        {
            Toast.makeText(activity!!, getString(R.string.success), Toast.LENGTH_SHORT).show()
        }
    }

    override fun failedRequest(msg: String, apiName: String) {
       return
    }




    override fun onRecyclItemLongClicked(viewID: Int, position: Int)
    {
        return
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int) {
        if(viewID== R.id.imageView)
        {

            try
            {
                MediaDetailsFragment.photoID = placePhotosModel.items!!.get(position).id.toString()
                MediaDetailsFragment.photoURL = placePhotosModel.items!!.get(position).uri.toString()
                MediaDetailsFragment.photoUserName = placePhotosModel.items!!.get(position).user.first_name+" "+placePhotosModel.items!!.get(position).user.last_name
                MediaDetailsFragment.photoUserImage = placePhotosModel.items!!.get(position).user.photo_uri.medium
                MediaDetailsFragment.photoDate = placePhotosModel.items!!.get(position).created_at.toString()
                MediaDetailsFragment.photoTitle = placePhotosModel.items!!.get(position).description.toString()
                MediaDetailsFragment.photoLikes = placePhotosModel.items!!.get(position).stats.likes.toString()

                replaceFragment(MediaDetailsFragment(), true)
            }

            catch (xx:Exception){}
        }

        if(viewID== R.id.selectedIMG)
        {
            try
            {

                placePhotosModel.premium_photo.uri= placePhotosModel.items!!.get(position).uri

                Glide.with(context).load(placePhotosModel.premium_photo.uri.medium.toString())
                        .into(binding.headerIMG);

                val scrollTo = (binding.headerIMG.getParent().getParent() as View).top
                binding.mainScroll.smoothScrollTo(0, scrollTo)

//                for(i in 0..placePhotosModel.items!!.size-1)
//                {
//                    if(i==position)
//                    {
//                        placePhotosModel.items!!.get(i).selected=true
//                    }
//                    else
//                    {
//                        placePhotosModel.items!!.get(i).selected=false
//                    }
//
//                }

                adapter.notifyDataSetChanged()

                submitPremiumImg(branchID,placePhotosModel.items!!.get(position).id.toString())
            }

            catch (xx:Exception)
            {}
        }
    }


    lateinit var binding: MediaFragmentBinding
    lateinit var placePhotosModel: PlacePhotosModel
    lateinit var branchesModel: com.jeeran.business.models.branches.BranchModel
    lateinit var adapter : MediaAdapter
    lateinit var  uploadMultiPart:UploadMultiPart
    lateinit var uploadeResponse: UploadMultiPart
    var branchID=""


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == 111){

            if (data != null)
            {
                loadingWheel.startSpinwheel(activity!!, false, false)
                Thread(Runnable {

                    var contentURI = data!!.data
                    try
                    {
//                        val bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, contentURI)
//
//                        var url= URLClass.multiPartsURL+branchID+"&user_id="+ sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY)
//                        val multipart = Multipart(URL(url),activity!!)
//                        var imagePath =File(multipart.getRealPathFromURI(contentURI,activity!!))
//
//                        //multipart.addFilePart("userfile",imagePath,"userfile","png")

                           UploadMultiPart.filePath= data.data
                           UploadMultiPart.UploadFileToServer().execute()




                    }
                    catch (e: IOException) {
                        loadingWheel.stopLoading()
                        e.printStackTrace()
                        Toast.makeText(context, "Check your permissions !", Toast.LENGTH_SHORT).show()
                    }

                }).start()


            }
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.media_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.media))



        getBranches()

        binding.branchSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?)
            {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                branchID=branchesModel.items!!.get(position).place.id.toString()
                getPlacePhotos(branchID)
            }

        }

        binding.addImgBTN.setOnClickListener(View.OnClickListener {

            uploadMultiPart=UploadMultiPart(activity!!,branchID,sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY))
            UploadMultiPart.uploadeResponse=this
            //UploadMultiPart.showFileChooser();

            selectImageInAlbum()
        })

        binding.editPremImgBTN.setOnClickListener(View.OnClickListener {

            for(i in 0..placePhotosModel.items!!.size-1)
            {
                if(placePhotosModel.premium_photo.id==placePhotosModel.items!!.get(i).id)
                {
                    placePhotosModel.items!!.get(i).selected=true
                    placePhotosModel.items!!.get(i).id=placePhotosModel.premium_photo.id

                    adapter.notifyDataSetChanged()


                    break
                }
            }
            val scrollTo = (binding.mediaLV.getParent().getParent() as View).bottom
            binding.mainScroll.smoothScrollTo(0, scrollTo)
        })

        return view
    }


    fun getPlacePhotos(branchID:String)
    {
        //var userID= sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY)
        RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.placePhotosURL+branchID+"/photos/",activity as Activity)
    }

    fun setPremiumChecked()
    {
        for(i in 0..placePhotosModel.items!!.size-1)
        {
            if(placePhotosModel.premium_photo.id==placePhotosModel.items!!.get(i).id)
            {
                placePhotosModel.items!!.get(i).selected=true
                placePhotosModel.items!!.get(i).id=placePhotosModel.premium_photo.id

                adapter.notifyDataSetChanged()


                break
            }
        }

    }
    fun getBranches()
    {
        var body= JSONObject()
        body.putOpt("user_id",sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.branchesURL,activity!!)

    }

    fun submitPremiumImg(placeID:String,photoID:String)
    {
        var body= JSONObject()
        body.putOpt("place_id",placeID)
        body.putOpt("photo_id",photoID)
        RequestHandler.getInstance(this).requestAPI("put", body, com.jeeran.business.networking.URLClass.submitPremiumImageURL,activity!!)

    }


    fun selectImageInAlbum()
    {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, 111)

    }
    fun takePhoto()
    {
        val intent1 = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent1.resolveActivity(AppController.getInstance().packageManager) != null) {
            startActivityForResult(intent1, 222)
        }
    }


}
