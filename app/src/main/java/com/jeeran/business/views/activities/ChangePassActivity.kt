package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.ChangePassDialogBinding
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import org.json.JSONObject

class ChangePassActivity : AppCompatActivity(), View.OnClickListener, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        Toast.makeText(applicationContext, R.string.success, Toast.LENGTH_SHORT).show()
        finish();
    }

    override fun failedRequest(msg: String, apiName: String) {
       return
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.submitBTN)
        {
           if(checkFields())
           {
               changePass(binding.oldPassTXT.text.toString().trim(),binding.confirmPassTXT.text.toString().trim())
           }

        }


    }

    lateinit var binding: ChangePassDialogBinding
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding= DataBindingUtil.setContentView(this, R.layout.change_pass_dialog)

        binding.submitBTN.setOnClickListener(this)
    }

    private fun checkFields(): Boolean {

        if (binding.oldPassTXT.text.toString().trim().equals("")) {
            Toast.makeText(applicationContext, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.newPassTXT.text.toString().trim().equals("")) {
            Toast.makeText(applicationContext, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.confirmPassTXT.text.toString().trim().equals("")) {
            Toast.makeText(applicationContext, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (!binding.newPassTXT.text.toString().trim().equals(binding.confirmPassTXT.text.toString().trim())) {
            Toast.makeText(applicationContext, R.string.password_not_match, Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }
    fun changePass(oldPass:String,newPass:String)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(this!!, appConstants.userID_KEY))
        body.putOpt("password", oldPass)
        body.putOpt("new_password", newPass)

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.changePassURL,this!!)

    }
}
