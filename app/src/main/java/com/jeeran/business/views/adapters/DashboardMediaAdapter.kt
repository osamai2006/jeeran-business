package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import kotlinx.android.synthetic.main.dashboard_media_item_row.view.*


class DashboardMediaAdapter(private var _items : com.jeeran.business.models.dashboard.DashboardModel, private var _callback:DashboardMediaAdapterCallback) :
        RecyclerView.Adapter<DashboardMediaViewHolder>()
{


    var  items : com.jeeran.business.models.dashboard.DashboardModel
    var callback: DashboardMediaAdapterCallback
    lateinit var  context:Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardMediaViewHolder
    {
        context=parent.context
        return DashboardMediaViewHolder(LayoutInflater.from(context).inflate(R.layout.dashboard_media_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: DashboardMediaViewHolder, position: Int)
    {
                    Glide.with(context).load(items.latest_photos!!.get(position).uri.medium.toString())
                            .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                    //.apply(RequestOptions.circleCropTransform())
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(holder.mediaIMG)


        holder.mediaIMG.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.mediaIMG,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.latest_photos!!.size
    }

}

 class DashboardMediaViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val mediaIMG = view.mediaIMG

}

 interface DashboardMediaAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}