package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeeran.business.R
import kotlinx.android.synthetic.main.dashboard_media_item_row.view.*


class SeeAllMediaAdapter(private var _items : com.jeeran.business.models.dashboard.DashboardModel, private var _callback:SeeAllMediaAdapterCallback) :
        RecyclerView.Adapter<SeeAllMediaViewHolder>()
{


    var  items : com.jeeran.business.models.dashboard.DashboardModel
    var callback: SeeAllMediaAdapterCallback
    lateinit var  context:Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeeAllMediaViewHolder
    {
        context=parent.context
        return SeeAllMediaViewHolder(LayoutInflater.from(context).inflate(R.layout.see_all_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: SeeAllMediaViewHolder, position: Int)
    {
                    Glide.with(context).load(items.latest_photos!!.get(position).uri.medium.toString())
                    //.apply(RequestOptions.circleCropTransform())
                    //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                    .into(holder.mediaIMG);


        holder.mediaIMG.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.mediaIMG,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.latest_photos!!.size
    }

}

 class SeeAllMediaViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val mediaIMG = view.mediaIMG

}

 interface SeeAllMediaAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}