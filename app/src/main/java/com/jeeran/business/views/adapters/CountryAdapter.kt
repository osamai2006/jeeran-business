package com.jeeran.business.views.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.models.countries.CountriesModel
import kotlinx.android.synthetic.main.countries_item_row.view.*


class CountryAdapter(private var _items : CountriesModel,private var _callback:CountryAdapterCallback) : RecyclerView.Adapter<CountryViewHolder>(), CitiesAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, cityID: Int, cityName: String) {

        callback.onRecyclItemClicked(R.id.cityTXT,cityID,cityName)
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        return
    }


    var callback: CountryAdapterCallback
     var  items : CountriesModel
    lateinit var context:Context
    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder
    {
        context=parent.context
        return CountryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.countries_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int)
    {


        holder?.countryTXT?.text = items.countries!!.get(position).name.toString()


        var adapter = CitiesAdapter(items.countries!!.get(position).cities,this)
        holder.citiesLV.adapter=adapter
        holder.citiesLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL ,false)


    }



    override fun getItemCount(): Int
    {
        return items.countries!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}

class CountryViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val countryTXT = view.countryTXT
    val citiesLV = view.citiesLV

}

interface CountryAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, cityID: Int,cityName: String)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}