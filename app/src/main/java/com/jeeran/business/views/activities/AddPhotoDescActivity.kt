package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.AddPhotoDescDialogBinding
import com.jeeran.business.databinding.ForgetPassDialogBinding
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.AppController
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import org.json.JSONObject

class AddPhotoDescActivity : AppCompatActivity(), RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String) {

        finish()
    }

    override fun failedRequest(msg: String, apiName: String) {
       return
    }




    lateinit var binding: AddPhotoDescDialogBinding

    companion object {
        var placeID=""
        var photoID=""
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding= DataBindingUtil.setContentView(this, R.layout.add_photo_desc_dialog)

        binding.submitBTN.setOnClickListener(View.OnClickListener {

            if (!binding.descTXT.text.toString().equals(""))
            {
                var body = JSONObject()
                body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(this!!, appConstants.userID_KEY))
                body.putOpt("place_id", placeID)
                body.putOpt("photo_id", photoID)
                body.putOpt("description", binding.descTXT.text.toString().trim())

                RequestHandler.getInstance(this).requestAPI("post", body, URLClass.bindPhotoURL, this)
           }

        })
    }
}
