package com.jeeran.business.views.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import com.jeeran.business.utils.appConstants
import kotlinx.android.synthetic.main.media_details_item_row.view.*


class MediaDetailsAdapter(private var _items : com.jeeran.business.models.mediaComments.MediaCommentsModel) :
        RecyclerView.Adapter<MediaDetailsViewHolder>()
{


    var  items : com.jeeran.business.models.mediaComments.MediaCommentsModel
    lateinit var context:Context
    init
    {
        items=_items

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaDetailsViewHolder
    {
        context=parent.context
        return MediaDetailsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.media_details_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: MediaDetailsViewHolder, position: Int)
    {
        holder?.userNameTXT?.text = items.items!!.get(position).user.first_name+" "+items.items!!.get(position).user.last_name
        holder?.bodyTXT?.text = items.items!!.get(position).content
        holder?.dateTXT?.text = appConstants.millisToDate(items.items!!.get(position).created_at.toLong())

        Glide.with(context).load(items.items!!.get(position).user.photo_uri)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                .into(holder.userIMG)
    }




    override fun getItemCount(): Int
    {
        return items.items!!.size
    }

}

class MediaDetailsViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val bodyTXT = view.bodyTXT
    val dateTXT = view.dateTXT
    val userNameTXT = view.userNameTXT
    val userIMG = view.userIMG
    val mainLO = view.mainLOY
}