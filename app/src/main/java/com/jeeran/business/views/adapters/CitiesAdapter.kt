package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import com.jeeran.business.models.countries.CitiesItem
import kotlinx.android.synthetic.main.cities_item_row.view.*


class CitiesAdapter(private var _items :  List<CitiesItem>?, private var _callback:CitiesAdapterCallback) :
        RecyclerView.Adapter<CitiesViewHolder>()
{


    var  items : List<CitiesItem>?
    var callback: CitiesAdapterCallback
    lateinit var  context: Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder
    {
        context=parent.context
        return CitiesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cities_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int)
    {
        holder.cityTXT.setText(items!!.get(position).name)

        holder.cityTXT.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.cityTXT,items!!.get(position).id,items!!.get(position).name)
        })


    }




    override fun getItemCount(): Int
    {
        return items!!.size
    }

}

 class CitiesViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val cityTXT = view.cityTXT


}

 interface CitiesAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, cityID: Int, cityName: String)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}