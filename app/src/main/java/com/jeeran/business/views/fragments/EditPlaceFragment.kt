package com.jeeran.business.views.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.location.*
import android.util.Log
import android.view.MotionEvent
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.*
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.lang.Exception
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.gson.Gson
import com.jeeran.business.databinding.EditPlaceFragmentBinding
import com.jeeran.business.utils.appConstants
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import com.jeeran.business.R
import com.jeeran.business.networking.URLClass
import com.jeeran.business.views.activities.CountriesActivity
import com.jeeran.business.views.activities.FeaturesActivity
import com.jeeran.business.views.activities.SubCategoriesActivity
import org.json.JSONArray


class EditPlaceFragment : BaseFragment(), OnMapReadyCallback, com.jeeran.business.networking.RequestHandler.ResponseHandler {


    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {

        if(apiName.equals(URLClass.catURL,true))
       {
           val gson = Gson()
           val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.category.CategoryModel::class.java)
           categoryModel = modelObj as com.jeeran.business.models.category.CategoryModel


           var spinnerItems :MutableList<String> = ArrayList()
           for (i in 0..categoryModel.items!!.size-1)
           {
               spinnerItems.add(categoryModel.items!!.get(i).name)
           }
           val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
           adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
           binding.categorySpin!!.setAdapter(adapter)

           for (i in 0..binding.categorySpin.adapter.getCount()-1)
           {
               if(businessModel.categories!!.get(0).id==categoryModel.items!!.get(i).id)
               {
                   binding.categorySpin.setSelection(i)
                   break
               }

           }
       }

       else     if(apiName.contains("regions/",true))
       {
           val gson = Gson()
           val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.regions.RegionModel::class.java)
           regionModel = modelObj as com.jeeran.business.models.regions.RegionModel


           var spinnerItems :MutableList<String> = ArrayList()
           for (i in 0..regionModel.items!!.size-1)
           {
               spinnerItems.add(regionModel.items!!.get(i).name)
           }
           val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
           adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
           binding.regionSpin!!.setAdapter(adapter)

//           for (i in 0..binding.regionSpin.adapter.getCount()-1)
//           {
//               if(businessModel.region.id==regionModel.items!!.get(i).id)
//               {
//                   binding.regionSpin.setSelection(i)
//                   break
//               }
//
//           }
       }
       else  if(apiName.equals(URLClass.businessDetailsURL+ businessID,true))
       {
           val gson = Gson()
           val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.MyBusiness.BusinessModel::class.java)
           businessModel = modelObj as com.jeeran.business.models.MyBusiness.BusinessModel

           binding.nameTXT.setText(businessModel.name)
           binding.placeNameEnTXT.setText(businessModel.name)
           binding.placeNameArTXT.setText(businessModel.name)
           binding.phone1TXT.setText(businessModel.tel!!.get(0).phone_number)
           binding.reviewTXT.setText(businessModel.stats.reviews.toString())
           binding.ratingBar.rating=businessModel.stats.average_rating.toFloat()
           binding.infoTXT.setText(businessModel.content.toString())
           binding.directionEngTXT.setText(businessModel.directions)
//           binding.directionArTXT.setText(businessModel.street)
          // binding.subCatSpin.setText(businessModel. .name)
           binding.citySpin.setText(businessModel.city.name)
           this.selectedCity=businessModel.city.id
           this.selectedRegion=businessModel.region.id
           this.selectedCat=businessModel.categories!!.get(0).id.toString()

           try{  binding.phone2TXT.setText(businessModel.tel!!.get(1).phone_number)}catch (xx:Exception){}
           try{  binding.phone3TXT.setText(businessModel.tel!!.get(2).phone_number)}catch (xx:Exception){}






//           for (i in 0..binding.subCategorySpin.adapter.getCount())
//           {
//               if(businessModel.categories!!.get(0).id==categoryModel.items!!.get(i).id)
//               {
//                   binding.categorySpin.setSelection(i)
//                   break
//               }
//
//           }


           Glide.with(context).load(businessModel.logo_uri.medium)
                   .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.no_image_ic))
                   //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                   .into(binding.logoIMG)


           try {

               placeLatLng=LatLng(businessModel.coordinates.lat,businessModel.coordinates.lon)

               if(placeLatLng!=null)
               {
                   mapFragment?.getMapAsync(this@EditPlaceFragment)

               }
           }
           catch (xx:Exception){}

           getCategory()
           getRegions(businessModel.city.id)

       }
       else     if(apiName.equals(com.jeeran.business.networking.URLClass.editBusinessURL,true))
       {
           fragmentManager!!.popBackStack()
       }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        if(mMap!=null)
        {

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLatLng, 16.0f))
                mMap.clear()
                mMap.addMarker(MarkerOptions().position(placeLatLng)
                        .icon(appConstants.bitmapDescriptorFromVector(context!!,R.drawable.map_pin_ic) )
                        .title("Your Branch")
                        .draggable(false))


            mMap.setOnCameraMoveListener {

                mMap.clear()
                // display imageView
                binding.imgLocationPinUp?.visibility = View.VISIBLE

            }

            mMap.setOnCameraIdleListener {

                // hiding imageView
               // binding.imgLocationPinUp?.visibility = View.GONE
                // customizing map marker with a custom icon
                // and place it on the current map camera position

                mMap.addMarker(MarkerOptions().position(mMap.cameraPosition.target)
                        .icon(appConstants.bitmapDescriptorFromVector(context!!,R.drawable.map_pin_ic) )
                        .title("Your Branch"))

                var geo: Geocoder =  Geocoder(getActivity(), Locale.getDefault());

                var addresses:List<Address> = geo.getFromLocation(mMap.cameraPosition.target.latitude,
                        mMap.cameraPosition.target.longitude, 1);

                if (addresses.size > 0)
                {

                    placeLatLng= LatLng(mMap.cameraPosition.target.latitude,mMap.cameraPosition.target.longitude)
                    //Toast.makeText(activity!!, addresses.get(0).getAddressLine(0), Toast.LENGTH_SHORT).show()
                }


            }


//
//           mMap.setOnMapLongClickListener {
//
//               var geo: Geocoder =  Geocoder(getActivity(), Locale.getDefault());
//               var addresses:List<Address> = geo.getFromLocation(it.latitude, it.longitude, 1);
//               if (addresses.size > 0) {
//
//                   placeLatLng= LatLng(it.latitude,it.longitude)
//                   mMap.clear()
//                   mMap.addMarker(MarkerOptions().position(placeLatLng).icon(appConstants.bitmapDescriptorFromVector(context!!,R.drawable.map_pin_ic) ).title("Your Branch"))
//                   Toast.makeText(activity!!, addresses.get(0).getAddressLine(0), Toast.LENGTH_SHORT).show()
//               }
//
//
//           }


        }
    }


    companion object{

        var businessID=""

    }





    lateinit var businessModel: com.jeeran.business.models.MyBusiness.BusinessModel
    lateinit var categoryModel: com.jeeran.business.models.category.CategoryModel
    lateinit var regionModel: com.jeeran.business.models.regions.RegionModel
    private lateinit var mMap: GoogleMap
    private lateinit var locationManager : LocationManager
    lateinit var  mapFragment:SupportMapFragment
    lateinit var placeLatLng: LatLng
    var selectedCat=""
    var selectedSubCatArr: JSONArray = JSONArray()
    var selectedFeaturedArr: JSONArray = JSONArray()
    var selectedCity=0
    var selectedRegion=0

    lateinit var binding: EditPlaceFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.edit_place_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root


        setScreenTitle(getString(R.string.manage_my_plac))
        initMap()
        getBusiness(businessID)



        binding.transparentImage.setOnTouchListener(object : View.OnTouchListener
        {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                val action = event?.getAction()
                when (action)
                {
                    MotionEvent.ACTION_DOWN -> {
                        binding.mainScrollVIew.requestDisallowInterceptTouchEvent(true);
                        return false;
                   }

                    MotionEvent.ACTION_UP -> {
                        binding.mainScrollVIew.requestDisallowInterceptTouchEvent(false);
                        return true;
                    }

                    MotionEvent.ACTION_MOVE -> {
                        binding.mainScrollVIew.requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                }

                return v?.onTouchEvent(event) ?: true
            }
        })


        binding.submitBTN.setOnClickListener(View.OnClickListener {

            if(checkFields())
            {

                addEditBusiness()
            }

        })

        binding.categorySpin.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                selectedCat=categoryModel.items!!.get(position).id.toString()
                binding.subCatSpin.setText("")
                selectedSubCatArr= JSONArray()

//                startActivityForResult(Intent(activity!!,SubCategoriesActivity::class.java),3030)

            }
        }

//        binding.subCategorySpin.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                return
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
//            {
//                selectedSubCat=subCatModel.items!!.get(position).id.toString()
//            }
//        }

        binding.subCatSpin.setOnClickListener(View.OnClickListener {

            SubCategoriesActivity.catID=selectedCat
            startActivityForResult(Intent(activity!!,SubCategoriesActivity::class.java),3030)
        })

        binding.featureSpin.setOnClickListener(View.OnClickListener {

            startActivityForResult(Intent(activity!!,FeaturesActivity::class.java),4040)
        })

        binding.citySpin.setOnClickListener(View.OnClickListener {

            startActivityForResult(Intent(activity!!,CountriesActivity::class.java),2020)
        })
//        binding.citySpin.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                return
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
//            {
//                selectedCity=citiesModel.items!!.get(position).id
//                getRegions(selectedCity)
//            }
//        }

        binding.regionSpin.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                selectedRegion=regionModel.items!!.get(position).id
            }
        }




        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==2020 && resultCode== Activity.RESULT_OK) {
            try
            {
                selectedCity = data!!.getIntExtra("cityID", 0)
                binding.citySpin.setText(data?.getStringExtra("cityName").toString())
                getRegions(selectedCity)
            }
            catch (xx:Exception)
            {
                xx.toString()
            }
        }

        if(requestCode==3030 && resultCode== Activity.RESULT_OK) {
            try
            {
                this.selectedSubCatArr=appConstants.selectedSubCatArr
                 binding.subCatSpin.setText(data?.getStringExtra("subCatNames").toString())

            }
            catch (xx:Exception)
            {
                xx.toString()
            }
        }

        if(requestCode==4040 && resultCode== Activity.RESULT_OK) {
            try
            {
                this.selectedFeaturedArr=appConstants.selectedFeaturArr
                binding.featureSpin.setText(data?.getStringExtra("fetureNames").toString())

            }
            catch (xx:Exception)
            {
                xx.toString()
            }
        }
    }

     @SuppressLint("MissingPermission")
     fun initMap() {

         try
         {
             locationManager = activity!!.getSystemService(LOCATION_SERVICE) as LocationManager;
             if(locationManager!==null)
             {
                 locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener);
             }
            mapFragment  = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

             


         }
         catch (ss:Exception)
         {
             Log.d("Mapp",ss.toString())
         }


    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location)
        {
//            if(addEditMode.equals("add",true))
//            {
//                currentLatLng = LatLng(location.latitude, location.longitude)
//                if (currentLatLng != null) {
//                    mapFragment?.getMapAsync(this@EditPlaceFragment)
//                }
//            }

        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

        }
        override fun onProviderEnabled(provider: String) {

        }
        override fun onProviderDisabled(provider: String) {

        }
    }

    fun addEditBusiness()
    {
        var phonesObjArr: JSONArray = JSONArray()
        phonesObjArr.put(binding.phone1TXT.text.toString().trim())
        phonesObjArr.put(binding.phone2TXT.text.toString().trim())
        phonesObjArr.put(binding.phone3TXT.text.toString().trim())
//        try
//        {
//            if(!binding.phone1TXT.text.toString().trim().equals("",true))
//            {
//                phonesObjArr.put(binding.phone1TXT.text.toString().trim())
//            }
//            else if(!binding.phone2TXT.text.toString().trim().equals("",true))
//            {
//                phonesObjArr.put(binding.phone1TXT.text.toString().trim())
//                phonesObjArr.put(binding.phone2TXT.text.toString().trim())
//            }
//            else if(!binding.phone3TXT.text.toString().trim().equals("",true))
//            {
//                phonesObjArr.put(binding.phone1TXT.text.toString().trim())
//                phonesObjArr.put(binding.phone2TXT.text.toString().trim())
//                phonesObjArr.put(binding.phone3TXT.text.toString().trim())
//            }
//
//        }
//
//
//
//        catch (xx:Exception)
//        {
//            xx.toString()
//        }

        var body= JSONObject()
        body.putOpt("place_name_en",binding.placeNameEnTXT.text.toString())
        body.putOpt("place_name_ar",binding.placeNameArTXT.text.toString())
        body.putOpt("category",selectedCat)
        body.putOpt("subcategories",this.selectedSubCatArr)
        body.putOpt("features",this.selectedFeaturedArr)
        body.putOpt("city",selectedCity)
        body.putOpt("region",selectedRegion)
        body.putOpt("tel",phonesObjArr)
        body.putOpt("additional_info",binding.infoTXT.text.toString().trim())
        body.putOpt("directions_ar",binding.directionArTXT.text.toString().trim())
        body.putOpt("directions_en",binding.directionEngTXT.text.toString().trim())
        body.putOpt("location",placeLatLng.latitude.toString()+","+placeLatLng.longitude.toString())

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.editBusinessURL,activity!!)

    }

    fun getBusiness(id:String)
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.businessDetailsURL+ businessID,activity!!)

    }



    fun getCategory()
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.catURL,activity!!)

    }
    fun getRegions(cityID:Int)
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.regionURL+cityID+"/regions/",activity!!)

    }

    private fun checkFields(): Boolean {

        if (binding.placeNameEnTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.phone1TXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.infoTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.directionEngTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.directionArTXT.text.toString().trim().equals("")) {
            Toast.makeText(activity!!, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }



}



