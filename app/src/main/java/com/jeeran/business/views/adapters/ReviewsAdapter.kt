package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import kotlinx.android.synthetic.main.reviews_item_row.view.*


 class ReviewsAdapter(private var _modelItems : com.jeeran.business.models.reviews.ReviewsModel, private var _callback:ReviewsAdapterCallback) :
        RecyclerView.Adapter<ReviewsViewHolder>()
{


    var  modelItems : com.jeeran.business.models.reviews.ReviewsModel
   lateinit var context:Context
    var callback: ReviewsAdapterCallback
    var earlyFlag=false

    init
    {
        modelItems=_modelItems
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewsViewHolder
    {
        context=parent.context
        return ReviewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.reviews_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: ReviewsViewHolder, position: Int)
    {

        if(position==0 && modelItems.items!!.get(position).is_read==false )
        {
            holder?.sectionTXT?.text = context.getString(R.string.new_)
            holder?.sectionTXT?.visibility=View.VISIBLE
            earlyFlag=false
            holder.readFlag.visibility=View.VISIBLE

        }
        else if(position>0 && modelItems.items!!.get(position).is_read==true && earlyFlag==false )
        {
            holder?.sectionTXT?.text = context.getString(R.string.earlier)
            holder?.sectionTXT?.visibility=View.VISIBLE
            earlyFlag=true
            holder.readFlag.visibility=View.INVISIBLE
        }
        else
        {
            holder?.sectionTXT?.visibility=View.GONE
            holder.readFlag.visibility=View.INVISIBLE
        }


        holder?.nameTXT?.text = modelItems.items!!.get(position).user.first_name+" "+modelItems.items!!.get(position).user.last_name
        holder?.reviewTXT?.text = modelItems.items!!.get(position).content
        holder?.countTXT?.text = modelItems.items!!.get(position).user.stats.reviews.toString()

        Glide.with(context).load(modelItems.items!!.get(position).user.photo_uri?.medium)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                .into(holder.userImg)

        holder.mainLO.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.mainLO,position)
        })
    }




    override fun getItemCount(): Int
    {
        return modelItems.items!!.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}

 class ReviewsViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val nameTXT = view.nameTXT
    val reviewTXT = view.reviewTXT
    val mainLO = view.mainLO
    val userImg = view.userImg
    val countTXT = view.countTXT
    val sectionTXT = view.sectionTXT
    val readFlag = view.readFlag

}

 interface ReviewsAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}