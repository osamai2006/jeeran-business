package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.MyPlacesFragmentBinding
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.MyPlacesAdapter
import com.jeeran.business.views.adapters.MyPlacesAdapterCallback
import org.json.JSONObject


class MyPlacesFragment : BaseFragment(), MyPlacesAdapterCallback, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String) {

        if(apiName.equals(com.jeeran.business.networking.URLClass.branchesURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.branches.BranchModel::class.java)
            branchesModel = modelObj as com.jeeran.business.models.branches.BranchModel


            var adapter = MyPlacesAdapter(branchesModel, this)
            binding.placesLV.adapter = adapter
            binding.placesLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }

        if(apiName.equals(com.jeeran.business.networking.URLClass.renewPlaceURL,true))
        {
            Toast.makeText(activity!!, R.string.success, Toast.LENGTH_SHORT).show()
        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int)
    {
        if(viewID==R.id.mainLO)
        {
            EditPlaceFragment.businessID=branchesModel.items!!.get(position).place.id.toString()
            replaceFragment(EditPlaceFragment(),true)
        }

        if(viewID==R.id.renewBTN)
        {
           renewPlace(branchesModel.items!!.get(position).place.id.toString())
        }
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int)
    {
       activity!!.supportFragmentManager.popBackStack()
    }

    lateinit var binding: MyPlacesFragmentBinding
    lateinit var branchesModel: com.jeeran.business.models.branches.BranchModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.my_places_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.my_places))

        getBusiness()

        binding.addBTN.setOnClickListener(View.OnClickListener {

            //replaceFragment(EditPlaceFragment(),true)
        })

        return view
    }

    fun getBusiness()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.branchesURL,activity!!)

    }

    fun renewPlace(placeID:String)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        body.putOpt("place_id", placeID)

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.renewPlaceURL,activity!!)

    }



}
