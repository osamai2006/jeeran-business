package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import com.jeeran.business.models.countries.CitiesItem
import com.jeeran.business.models.features.ItemsItem
import kotlinx.android.synthetic.main.cities_item_row.view.*
import kotlinx.android.synthetic.main.features_sub_item_row.view.*


class FeatureSubAdapter(private var _items :  List<ItemsItem>?, private var _callback:FeatureSubAdapterCallback) :
        RecyclerView.Adapter<FeatureSubViewHolder>()
{


    var  items : List<ItemsItem>?
    var callback: FeatureSubAdapterCallback
    lateinit var  context: Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatureSubViewHolder
    {
        context=parent.context
        return FeatureSubViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.features_sub_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: FeatureSubViewHolder, position: Int)
    {
        holder.featureNameView.setText(items!!.get(position).name)

        holder.selectedFeatureIMG.setOnClickListener(View.OnClickListener {

            if(items!!.get(position).selected==true)
            {
                items!!.get(position).selected=false
                holder.selectedFeatureIMG.setImageResource(R.drawable.unselect_ic)

            }
            else
            {
                items!!.get(position).selected=true
                holder.selectedFeatureIMG.setImageResource(R.drawable.selected_ic)
            }

           // callback.onRecyclItemClicked(R.id.cityTXT,items!!.get(position).id,items!!.get(position).name)
        })


    }




    override fun getItemCount(): Int
    {
        return items!!.size
    }

}

 class FeatureSubViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val selectedFeatureIMG = view.selectedFeatureIMG
    val featureNameView = view.featureNameView


}

 interface FeatureSubAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, featureID: Int, featureName: String)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}