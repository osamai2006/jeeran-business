package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import com.jeeran.business.models.placePhotos.PlacePhotosModel
import kotlinx.android.synthetic.main.media_item_row.view.*


 class MediaAdapter(private var _items : com.jeeran.business.models.placePhotos.PlacePhotosModel, private var _callback:MediaAdapterCallback) :
        RecyclerView.Adapter<MediaViewHolder>()
{


    var  items : PlacePhotosModel
    var callback: MediaAdapterCallback
    lateinit var  context: Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder
    {
        context=parent.context
        return MediaViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.media_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int)
    {

        Glide.with(context).load(items.items!!.get(position).uri.medium)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                .into(holder.imageView);

        if(items.items!!.get(position).selected==false)
        {
            holder.selectedIMG.setImageResource(R.drawable.unselect_ic)

        }
        else
        {
            holder.selectedIMG.setImageResource(R.drawable.selected_ic)
        }

        holder.imageView.setOnClickListener(View.OnClickListener {

            callback.onRecyclItemClicked(R.id.imageView,position)
        })

        holder.selectedIMG.setOnClickListener(View.OnClickListener {

            if(items.items!!.get(position).selected==true)
            {
                items.items!!.get(position).selected=false

            }
            else
            {
                items.items!!.get(position).selected=true
            }

            for(i in 0..items.items!!.size-1)
            {
                if(i==position)
                {
                    items.items!!.get(i).selected=true
                }
                else
                {
                    items.items!!.get(i).selected=false
                }

            }

            callback.onRecyclItemClicked(R.id.selectedIMG,position)
        })
    }




    override fun getItemCount(): Int
    {
        return items.items!!.size
    }

}

 class MediaViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val imageView = view.imageView
    val selectedIMG = view.selectedIMG

}

 interface MediaAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}