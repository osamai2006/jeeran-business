package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.ForgetPassDialogBinding
import com.jeeran.business.utils.AppController
import org.json.JSONObject

class ForgetPassActivity : AppCompatActivity(), com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String) {
        finish()
    }

    override fun failedRequest(msg: String, apiName: String) {
       return
    }




    lateinit var binding: ForgetPassDialogBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding= DataBindingUtil.setContentView(this, R.layout.forget_pass_dialog)

        binding.submitBTN.setOnClickListener(View.OnClickListener {

            if (android.util.Patterns.EMAIL_ADDRESS.matcher(binding.emailTXT.text.toString()).matches())
            {
                var body = JSONObject()
                body.putOpt("email", binding.emailTXT.text.toString().trim())

                com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.forgetPassURL, this)
           }
            else
            {
                Toast.makeText(applicationContext, R.string.invalid_email, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
