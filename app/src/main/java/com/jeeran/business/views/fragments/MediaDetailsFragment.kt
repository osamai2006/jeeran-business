package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.MediaDetailFragmentBinding
import com.jeeran.business.models.mediaComments.MediaCommentsModel
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.MediaAdapterCallback
import com.jeeran.business.views.adapters.MediaDetailsAdapter
import org.json.JSONObject
import java.lang.Exception


class MediaDetailsFragment : BaseFragment(), MediaAdapterCallback, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.contains("photos",true) && requestType.equals("get",true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.mediaComments.MediaCommentsModel::class.java)
            mediaCommentsModel = modelObj as MediaCommentsModel

            var adapter = MediaDetailsAdapter(mediaCommentsModel)
            binding.commentsLV.adapter=adapter
            binding.commentsLV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL ,false)
            (binding.commentsLV.layoutManager as LinearLayoutManager).setReverseLayout(true);

//            binding.commentsLV.requestFocus()
//            binding.commentsLV.smoothScrollToPosition(mediaCommentsModel.items!!.size+2)

            if(mediaCommentsModel.items!!.size>0)
            {
                binding.commentsTXT.setText(mediaCommentsModel.items!!.size.toString())
            }
        }

        if(apiName.contains("photos",true) && requestType.equals("post",true))
        {
            getPhotosComments(photoID)
        }

        return
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }


    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
      return
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int) {
        return
    }


    lateinit var binding: MediaDetailFragmentBinding
    lateinit var mediaCommentsModel: MediaCommentsModel

    companion object
    {
        var photoID:String=""
        var photoURL:String?=""
        var photoDate:String=""
        var photoUserName:String?=""
        var photoUserImage:String?=""
        var photoTitle:String?=""
        var photoLikes:String?=""
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.media_detail_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.media))
        getPhotosComments(photoID)

        binding.nameTXT.setText(photoUserName)
        try{ binding.dateTXT.setText(appConstants.millisToDate(photoDate.toLong()))}catch (xx:Exception){}
        binding.likesTXT.setText(photoLikes)
        binding.imgTitleTXT.setText(photoTitle)

        Glide.with(context).load(photoUserImage)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                .into(binding.userIMG)

        Glide.with(context).load(photoURL)
               // .apply(RequestOptions.centerInsideTransform().placeholder(R.drawable.no_image_ic))
                .into(binding.photoIMGV)


        binding.sendBTN.setOnClickListener(View.OnClickListener {

            if(!binding.msgTXT.text.toString().equals("",true))
            {
                submitComments(photoID,binding.msgTXT.text.toString())
                binding.msgTXT.setText("")
            }
        })


        return view
    }

     fun fillCommentsList()
    {
//        var items: MutableList<NotificationModel> = ArrayList()
//        lateinit var  model:NotificationModel
//        lateinit var adapter:MediaDetailsAdapter
//
//        for(i in 0..15)
//        {
//            model =  NotificationModel("business-amman",1,"descrip  descrip ",true,1)
//            items.add(model)
//        }
//        adapter =MediaDetailsAdapter(items)
//        binding.commentsLV.adapter=adapter
//        binding.commentsLV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL ,false)


    }

    fun getPhotosComments(photoID:String)
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.getPhotosCommentsURL+photoID+"/comments/",activity!!)

    }

    fun submitComments(photoID:String,comment:String)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!,appConstants.userID_KEY))
        body.putOpt("content",comment)
        body.putOpt("publish_status","1")
        body.putOpt("photo_id",photoID)
        body.putOpt("platform","Android App")
        body.putOpt("created_at","0")

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.addPhotosCommentsURL+photoID+"/comments/",activity!!)

    }




}
