package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeeran.business.R
import com.jeeran.business.databinding.SeeAllFragmentBinding
import com.jeeran.business.views.adapters.SeeAllMediaAdapter
import com.jeeran.business.views.adapters.SeeAllMediaAdapterCallback
import java.lang.Exception


class SeeAllFragment : BaseFragment(),View.OnClickListener, SeeAllMediaAdapterCallback {


    override fun onRecyclItemClicked(viewID: Int, position: Int)
    {

        if(viewID==R.id.mediaIMG)
        {

            try
            {
                MediaDetailsFragment.photoID = dashboardModel.latest_photos!!.get(position).id.toString()
                MediaDetailsFragment.photoURL = dashboardModel.latest_photos!!.get(position).uri.medium.toString()
                MediaDetailsFragment.photoUserName = dashboardModel.latest_photos!!.get(position).user?.first_name+" "+dashboardModel.latest_photos!!.get(position).user?.last_name
                MediaDetailsFragment.photoUserImage = dashboardModel.latest_photos!!.get(position).user?.photo_uri
                MediaDetailsFragment.photoDate = dashboardModel.latest_photos!!.get(position).created_at.toString()
                MediaDetailsFragment.photoTitle =dashboardModel.latest_photos!!.get(position).description.toString()
                MediaDetailsFragment.photoLikes = dashboardModel.latest_photos!!.get(position).stat_likes.toString()

                replaceFragment(MediaDetailsFragment(), true)
            }

            catch (xx: Exception){}
        }

    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int)
    {
        return
    }

    override fun onClick(v: View?)
    {


    }


    lateinit var binding: SeeAllFragmentBinding



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.see_all_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.media))

        var adapter = SeeAllMediaAdapter(dashboardModel,this)
        binding.allMediaLV.adapter=adapter
        binding.allMediaLV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL ,false)



        return view
    }


    companion object{

        lateinit var dashboardModel: com.jeeran.business.models.dashboard.DashboardModel
    }







}
