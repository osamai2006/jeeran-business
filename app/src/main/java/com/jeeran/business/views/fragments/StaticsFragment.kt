package com.jeeran.business.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.content.Intent
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.StaticsFragmentBinding
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.activities.CustomDateActivity
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap




class StaticsFragment : BaseFragment(),  RequestHandler.ResponseHandler, View.OnClickListener {
    override fun onClick(v: View?) {

        if(v!!.id==R.id.dateTXT)
        {
            startActivityForResult(Intent(activity!!, CustomDateActivity::class.java),111)
        }
    }

    override fun successRequest(response: JSONObject, apiName: String,requestType: String) {

        if(apiName.equals( URLClass.branchesURL,true))
        {

            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.branches.BranchModel::class.java)
            branchesModel = modelObj as com.jeeran.business.models.branches.BranchModel


            var spinnerItems :MutableList<String> = ArrayList()
            for (i in 0..branchesModel.items!!.size-1)
            {
                spinnerItems.add(branchesModel.items!!.get(i).place.name+" - "+branchesModel.items!!.get(i).place.region.name)
            }
            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.branchSpin!!.setAdapter(adapter)
        }

        if(apiName.equals( URLClass.getChartFiltersURL,true))
        {

            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.chartFilters.ChartFiltersModel::class.java)
            chartFiltersModel = modelObj as com.jeeran.business.models.chartFilters.ChartFiltersModel


            var spinnerItems :MutableList<String> = ArrayList()
            spinnerItems.add(getString(R.string.all))
            for (i in 0..chartFiltersModel.filters!!.size-1)
            {
                spinnerItems.add(chartFiltersModel.filters!!.get(i).name.toString())
            }
            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerItems)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.filterSpin!!.setAdapter(adapter)
        }

        if(apiName.equals( URLClass.getStatisticsURL,true))
        {

            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.charts.ChartModel::class.java)
            chartModel = modelObj as com.jeeran.business.models.charts.ChartModel

            if(chartModel.events!!.size>2)
            {
                initilizeChart()
                 binding.totalTXT.text=resources.getString(R.string.total)+" : "+chartModel.events!!.size.toString()
            }
            else
            {
                binding.mChart.clear()
                binding.totalTXT.text=resources.getString(R.string.total)+" : 0"
            }

        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    lateinit var binding: StaticsFragmentBinding
    //lateinit var chartArrayList:List<NotificationModel>
    lateinit var chartModel: com.jeeran.business.models.charts.ChartModel
    lateinit var branchesModel: com.jeeran.business.models.branches.BranchModel
    lateinit var chartFiltersModel: com.jeeran.business.models.chartFilters.ChartFiltersModel
    var sDate="";
    var eDate="";
    var placeID=""
    var filterID="0"

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111)
        {
            try
            {
                sDate=data!!.extras.getString("sDate")
                eDate=data!!.extras.getString("eDate")

                binding.dateTXT.setText(sDate+" - "+eDate)

                getChart(placeID,sDate,eDate,filterID)
            }
            catch (xx:java.lang.Exception)
            {

            }

        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.statics_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

//        binding.chart.setProgress(85.5f,true);
//        binding.chart2.setProgress(25.5f,true);
//        binding.chart3.setProgress(40.5f,true);
        binding.dateTXT.setOnClickListener(this)

        setScreenTitle(resources.getString(R.string.statics))

        binding.dateTXT.setText(getPastMonth()+" - "+getCurrentDate())
        sDate=getPastMonth()
        eDate=getCurrentDate()

        getBranches()


        binding.filterSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?)
            {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                if(position==0)
                {
                    filterID="0"
                }
                else {
                    filterID = chartFiltersModel.filters!!.get(position-1).id.toString()
                }
                
                getChart(placeID,sDate,eDate,filterID)
            }

        }

        binding.branchSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                 placeID=branchesModel.items!!.get(position).id.toString()
                 getChartFilters()

            }
        }

        return view
    }




    private fun initilizeChart() {

        try {
            binding.mChart.setViewPortOffsets(0f, 0f, 0f, 0f)
            binding.mChart.setBackgroundColor(Color.TRANSPARENT)

            // no description text
            binding.mChart.getDescription().setEnabled(false)

            // enable touch gestures
            binding.mChart.setTouchEnabled(true)

            // enable scaling and dragging
            binding.mChart.setDragEnabled(true)
            binding.mChart.setScaleEnabled(true)
            binding.mChart.isDragXEnabled()
            binding.mChart.isDragYEnabled()
            // if disabled, scaling can be done on x- and y-axis separately
            binding.mChart.setPinchZoom(true)

            binding.mChart.setDrawGridBackground(false)
            binding.mChart.setMaxHighlightDistance(300f)

            val dateMap = HashMap<Int, String>()
            for (x in 0..  chartModel.events!!.size-1)
            {
                //val dateArr = chartArrayList[x].regionCode.split(" ")
                dateMap[x] = chartModel.events!!.get(x).date.toString()            }

            val x = binding.mChart.getXAxis()
            x.setLabelCount(chartModel.events!!.size, false)
            x.setTextColor(resources.getColor(R.color.black))
            x.setTextSize(9f)
            x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE)
            x.setDrawGridLines(true)
            x.setAxisLineColor(Color.GREEN)
            x.setGranularity(5f)
            x.setValueFormatter(IAxisValueFormatter { value, axis ->
                dateMap[value.toInt()]
            })


            val y = binding.mChart.getAxisLeft()
            y.setLabelCount( chartModel.events!!.size, false)
            y.setTextColor(Color.BLACK)
            y.setTextSize(11f)
            //y.setGranularity(5f)
            y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
            //y.setDrawGridLines(true)
            y.setAxisLineColor(resources.getColor(R.color.green))

            binding.mChart.getAxisRight().setEnabled(false)

            // add data
            setData()

            binding.mChart.getLegend().setEnabled(false)

            binding.mChart.animateXY(1000, 1000)

            // dont forget to refresh the drawing
            binding.mChart.invalidate()
        } catch (xx: Exception) {

            xx.message.toString()
        }

    }


    private fun setData() {

        try {


            val yVals = ArrayList<Entry>()

            for (i in 0.. chartModel.events!!.size-1) {
                // float mult = (range + 1);
                val value = java.lang.Float.valueOf(chartModel.events!!.get(i).value.toFloat()) //(float) (Math.random() * mult) + 20;// + (float)
                // ((mult *
                // 0.1) / 10);
                yVals.add(Entry(i.toFloat(), value))
            }

            val set1: LineDataSet

            if (binding.mChart.getData() != null && binding.mChart.getData().getDataSetCount() > 0) {
                set1 = binding.mChart.getData().getDataSetByIndex(0) as LineDataSet
                set1.values = yVals
                binding.mChart.getData().notifyDataChanged()
                binding. mChart.notifyDataSetChanged()
            } else {
                // create a dataset and give it a type
                set1 = LineDataSet(yVals, "")

                set1.mode = LineDataSet.Mode.CUBIC_BEZIER
                set1.cubicIntensity = 0.3f
                //set1.setDrawFilled(true);
                set1.setDrawCircles(false)
                set1.lineWidth = 3.0f
                set1.circleRadius = 3f
                set1.setCircleColor(resources.getColor(R.color.green))
                //set1.highLightColor = Color.rgb(244, 117, 117)
                set1.color = resources.getColor(R.color.green)
                set1.fillColor = resources.getColor(R.color.white_dark)
                set1.fillAlpha = 100
                set1.setDrawHorizontalHighlightIndicator(false)
                set1.fillFormatter = IFillFormatter { dataSet, dataProvider -> -10f }

                // create a data object with the datasets
                val data = LineData(set1)
                data.setValueTextSize(10f)
                data.setDrawValues(false)

                // set data
                binding.mChart.setData(data)

                for (set in binding.mChart.getData().getDataSets()) {
                    set.setDrawValues(!set.isDrawValuesEnabled())
                    set.setValueTextColor(resources.getColor(R.color.green))
                }


                val sets = binding.mChart.getData().getDataSets()
                for (iSet in sets) {

                    val set = iSet as LineDataSet

                    if (set.isDrawFilledEnabled)
                        set.setDrawFilled(false)
                    else
                        set.setDrawFilled(true)
                }


                for (iSet in sets) {

                    val set = iSet as LineDataSet
                    if (set.isDrawCirclesEnabled)
                        set.setDrawCircles(false)
                    else
                        set.setDrawCircles(true)
                }

                // mChart.invalidate();
            }
        } catch (xx: Exception) {
            xx.message
        }

    }

    fun getBranches()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
         RequestHandler.getInstance(this).requestAPI("post", body,  URLClass.branchesURL,activity!!)

    }

    fun getChartFilters()
    {
         RequestHandler.getInstance(this).requestAPI("get", null,  URLClass.getChartFiltersURL,activity!!)

    }

    fun getChart(placeID:String,sDate:String,eDate:String,eventID:String)
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        body.putOpt("place_id", "168395")
        body.putOpt("date_from", sDate)
        body.putOpt("date_to", eDate)
        body.putOpt("event_id", eventID.toInt())

        RequestHandler.getInstance(this).requestAPI("post", body,  URLClass.getStatisticsURL,activity!!)

    }

    fun getCurrentDate():String
    {
        var currDate=""

        val c = Calendar.getInstance().time
        val df = java.text.SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
        currDate = df.format(c)

        return  currDate
    }

    fun getPastMonth():String
    {
        var pastDate=""

        val c = Calendar.getInstance()
        val df = java.text.SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
        c.add(Calendar.MONTH, -1);
        pastDate=df.format(c.getTime());

        return  pastDate
    }

}
