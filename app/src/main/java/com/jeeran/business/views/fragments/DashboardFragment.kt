package com.jeeran.business.views.fragments

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup



import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.DashboardFragmentBinding
import com.jeeran.business.models.notification.NotificationCountModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.networking.URLClass
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.DashboardMediaAdapter
import com.jeeran.business.views.adapters.DashboardMediaAdapterCallback
import org.json.JSONObject
import java.lang.Exception


class DashboardFragment : BaseFragment(),View.OnClickListener, DashboardMediaAdapterCallback, com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        if(apiName.equals(URLClass.getDashBoardURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.dashboard.DashboardModel::class.java)
            dashboardModel = modelObj as com.jeeran.business.models.dashboard.DashboardModel

            appConstants.getDashboardCalled=true
            fillDashboard()

        }

        if(apiName.equals(URLClass.getNotificationsCountURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), NotificationCountModel::class.java)
            var notificationCountModel = modelObj as NotificationCountModel

            appConstants.getDashboardCalled=true


           setNotificCount(notificationCountModel.notifications_count.toString())


        }

    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int)
    {

        if(viewID==R.id.mediaIMG)
        {

            try
            {
                MediaDetailsFragment.photoID = dashboardModel.latest_photos!!.get(position).id.toString()
                MediaDetailsFragment.photoURL = dashboardModel.latest_photos!!.get(position).uri.medium.toString()
                MediaDetailsFragment.photoUserName = dashboardModel.latest_photos!!.get(position).user?.first_name.toString()+" "+dashboardModel.latest_photos!!.get(position).user?.last_name.toString()
                MediaDetailsFragment.photoUserImage = dashboardModel.latest_photos!!.get(position).user?.photo_uri.toString()
                MediaDetailsFragment.photoDate = dashboardModel.latest_photos!!.get(position).created_at.toString()
                MediaDetailsFragment.photoTitle =dashboardModel.latest_photos!!.get(position).description.toString()
                MediaDetailsFragment.photoLikes = dashboardModel.latest_photos!!.get(position).stat_likes.toString()

                replaceFragment(MediaDetailsFragment(), true)
            }

            catch (xx: Exception) {
            Crashlytics.logException(xx)
                xx.toString()
            }
        }

    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int)
    {
        return
    }

    override fun onClick(v: View?)
    {
        try {


            if (v!!.id == R.id.reviewBTN) {
                replaceFragment(ReviewsFragment(), true)
            }
            if (v!!.id == R.id.staticsBTN) {
                replaceFragment(StaticsFragment(), true)
            }
            if (v!!.id == R.id.editMyBusinsBTN) {
                replaceFragment(MyPlacesFragment(), true)
            }
            if (v!!.id == R.id.editMediaBTN) {
                replaceFragment(MediaFragment(), true)
            }
            if (v!!.id == R.id.seeAllBTN)
            {
                if (dashboardModel.latest_photos!!.size >= 1) {
                    SeeAllFragment.dashboardModel = this.dashboardModel
                    replaceFragment(SeeAllFragment(), true)
                }
            }
        }
        catch (xx:Exception)
        {

        }
    }


    lateinit var binding: DashboardFragmentBinding

    lateinit var dashboardModel: com.jeeran.business.models.dashboard.DashboardModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.home_dr))

        appConstants.userToken=sharedPrefs.getInstance().getStringPreference(context!!,appConstants.userToken_KEY)
        binding.reviewBTN.setOnClickListener(this)
        binding.staticsBTN.setOnClickListener(this)
        binding.editMyBusinsBTN.setOnClickListener(this)
        binding.editMediaBTN.setOnClickListener(this)
        binding.seeAllBTN.setOnClickListener(this)

        getNotificatoinsCount()

        if(appConstants.getDashboardCalled==false)
        {


            getDashboard()
        }
        else
        {
            fillDashboard()
        }



        var token=sharedPrefs.getInstance().getStringPreference(context!!,appConstants.mobileToken_KEY)
        return view
    }



    private fun getDashboard()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.getDashBoardURL,activity as Activity)


    }


    private fun fillDashboard()
    {
        try
        {
            binding.unseenReviwsTXT.text = dashboardModel.reviews!!.unseen.toString()


            binding.unseenStatsTXT.text = dashboardModel.statistics!!.today.toString()

            var adapter = DashboardMediaAdapter(dashboardModel, this)
            binding.mediaLV.adapter = adapter
            binding.mediaLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)


            if (appConstants.dahsboardAnimationFinished == false) {
                val animator = ValueAnimator.ofInt(0, dashboardModel.reviews!!.total)
                animator.duration = 1500
                animator.addUpdateListener { animation -> binding.totalReviewsTXT.setText(animation.animatedValue.toString()) }
                animator.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        // binding.unseenReviwsTXT.setText(dashboardModel.reviews!!.unseen.toString())

                    }
                })
                animator.start()

                val animator2 = ValueAnimator.ofInt(0, dashboardModel.statistics!!.total)
                animator2.duration = 1500
                animator2.addUpdateListener { animation -> binding.totalStatsTXT.setText(animation.animatedValue.toString()) }
                animator2.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        // binding.unseenReviwsTXT.setText(dashboardModel.reviews!!.unseen.toString())

                    }
                })
                animator2.start()
                appConstants.dahsboardAnimationFinished = true

            } else {
                binding.totalStatsTXT.setText(dashboardModel.statistics!!.total.toString())
                binding.totalReviewsTXT.setText(dashboardModel.reviews!!.total.toString())
            }
        }
        catch (xx:Exception)
        {

        }
    }

    fun getNotificatoinsCount()
    {
        var body= JSONObject()
        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))

       RequestHandler.getInstance(this).requestAPI("post", body, URLClass.getNotificationsCountURL,activity as Activity)
    }


}
