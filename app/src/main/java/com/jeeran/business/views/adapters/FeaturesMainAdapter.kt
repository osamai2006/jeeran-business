package com.jeeran.business.views.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.models.countries.CountriesModel
import com.jeeran.business.models.features.FeaturesModel
import kotlinx.android.synthetic.main.countries_item_row.view.*
import kotlinx.android.synthetic.main.feature_main_item_row.view.*


class FeaturesMainAdapter(private var _items : FeaturesModel,private var _callback:FeaturesAdapterCallback) : RecyclerView.Adapter<FeaturesViewHolder>(), FeaturesAdapterCallback, FeatureSubAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, fetureID: Int, fetureName: String)
    {
        callback.onRecyclItemClicked(R.id.cityTXT,fetureID,fetureName)
    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
       return
    }



    var callback: FeaturesAdapterCallback
     var  items : FeaturesModel
    lateinit var context:Context
    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturesViewHolder
    {
        context=parent.context
        return FeaturesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.feature_main_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: FeaturesViewHolder, position: Int)
    {


        holder?.featureTXT?.text = items.items!!.get(position).name.toString()


        var adapter = FeatureSubAdapter(items.items!!.get(position).items,this)
        holder.subFeatureLV.adapter=adapter
        holder.subFeatureLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL ,false)


    }



    override fun getItemCount(): Int
    {
        return items.items!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}

class FeaturesViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val featureTXT = view.featureTXT
    val subFeatureLV = view.subFeatureLV

}

interface FeaturesAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, fetureID: Int,fetureName: String)
    fun onRecyclItemLongClicked(viewID: Int, position: Int)
}