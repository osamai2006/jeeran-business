package com.jeeran.business.views.activities

import android.os.Bundle


import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.RequestRegistraActivityBinding
import com.jeeran.business.utils.AppController

import org.json.JSONObject
import java.lang.Exception
import androidx.annotation.NonNull
import android.R.id
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener


class RequestRegistrationActivity : BaseActivity(), com.jeeran.business.networking.RequestHandler.ResponseHandler {
    override fun successRequest(response: JSONObject, apiName: String,requestType: String)
    {
        finish()
    }

    override fun failedRequest(msg: String, apiName: String)
    {
         return
    }


    lateinit var binding: RequestRegistraActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        try {


            super.onCreate(savedInstanceState)
            AppController.getInstance().setCurrentActivity(this)
            binding = DataBindingUtil.setContentView(this, R.layout.request_registra_activity)
            binding.lifecycleOwner = this

            hideNavigationBarVisibility(true)

            init()
        }
        catch (xx:Exception)
        {
            xx.toString()
        }

    }


    fun init()
    {
        lifecycle.addObserver(binding.youtubePlayerView)

       // var fullScreenHelper:FullScreenHelper = FullScreenHelper()

     //   fullScreenHelper.enterFullScreen(binding.youtubePlayerView)

//        binding.youtubePlayerView.initialize(YouTubePlayerInitListener { initializedYouTubePlayer ->
//            initializedYouTubePlayer.addListener(object : AbstractYouTubePlayerListener() {
//                override fun onReady() {
//                    initializedYouTubePlayer.loadVideo("ZDL0U7CCMMI", 0f)
//                }
//            })
//        }, true)


        binding.youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
              override   fun onReady(youTubePlayer: YouTubePlayer)
              {
                val videoId = "ZDL0U7CCMMI"
                youTubePlayer.loadVideo(videoId, 0f)
            }
        })

        binding.submitBTN.setOnClickListener(View.OnClickListener {

            if(checkFields())
            {
                var body= JSONObject()
                body.putOpt("place_name",binding.placeNameTXT.text.toString().trim())
                body.putOpt("first_name",binding.fNameTXT.text.toString().trim())
                body.putOpt("last_name",binding.lNameTXT.text.toString().trim())
                body.putOpt("phone_number",binding.phoneTXT.text.toString().trim())
                body.putOpt("email",binding.emailTXT.text.toString().trim())

                com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post",body, com.jeeran.business.networking.URLClass.registerURL,this)
            }
        })
    }

    fun checkFields():Boolean
    {
        if (binding.placeNameTXT.text.toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        if (binding.fNameTXT.text.toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        if (binding.lNameTXT.text.toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        if (binding.phoneTXT.text.toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        if (binding.emailTXT.text.toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show()
            return false
        }

        if ( !Patterns.EMAIL_ADDRESS.matcher(binding.emailTXT.text.toString().trim()).matches())
        {
            Toast.makeText(this, R.string.invalid_email, Toast.LENGTH_SHORT).show()
            return false
        }


        return true

    }



}
