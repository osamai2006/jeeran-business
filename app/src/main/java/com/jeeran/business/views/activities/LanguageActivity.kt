package com.jeeran.business.views.activities


import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.jeeran.business.R
import com.jeeran.business.databinding.LanguageDialogBinding
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import java.lang.Exception
import java.util.*

class LanguageActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        if (v?.id == R.id.engBTN)
        {
            setInitialLanguage("en")
           // finish()
        }

        if (v?.id == R.id.arBTN)
        {
            setInitialLanguage("ar")
           // finish()
        }
    }

    lateinit var binding: LanguageDialogBinding
    override fun onCreate(savedInstanceState: Bundle?)
    {
        try {
            super.onCreate(savedInstanceState)
            binding = DataBindingUtil.setContentView(this, R.layout.language_dialog)

            binding.engBTN.setOnClickListener(this)
            binding.arBTN.setOnClickListener(this)
        }
        catch (xx:Exception)
        {
            Log.d("languageActivtyOnCrate",xx.toString())
        }
    }


    private fun setInitialLanguage(language: String)
    {
        val locale = Locale(language)
        val res = getApplicationContext().getResources()
        val dm = res.getDisplayMetrics()
        val conf = res.getConfiguration()
        conf.locale = locale
        res.updateConfiguration(conf, dm)
        onConfigurationChanged(conf)

        if (language.equals("ar", ignoreCase = true))
        {
            appConstants.current_language = "ar"
        } else {
            appConstants.current_language = "en"
        }

        sharedPrefs.getInstance().setStringPreference(getApplicationContext(), appConstants.language_KEY, language)
        appConstants.getDashboardCalled=false
        appConstants.getBusinessCalled=false
        appConstants.dahsboardAnimationFinished=false


        val i = getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName())
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
        startActivity(i)


    }
}
