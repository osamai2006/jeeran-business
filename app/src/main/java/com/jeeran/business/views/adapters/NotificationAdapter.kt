package com.jeeran.business.views.adapters


import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.jeeran.business.R
import com.jeeran.business.utils.appConstants
import kotlinx.android.synthetic.main.notification_item_row.view.*


 class NotificationAdapter(private var _items : com.jeeran.business.models.notification.NotificationModel) :
        RecyclerView.Adapter<NotificationViewHolder>()
{
     var  items : com.jeeran.business.models.notification.NotificationModel
    lateinit var context:Context
     var earlyFlag=false
    init
    {
        items=_items

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder
    {
        context=parent.context
        return NotificationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int)
    {
        if(position==0 && items.events!!.get(position).is_read==0 )
        {
            holder.sectionTXT.visibility=View.VISIBLE
            holder?.sectionTXT?.text = context.getString(R.string.new_)
            earlyFlag=false


        }
        else if(position>0 && items.events!!.get(position).is_read==1 && earlyFlag==false )
        {
            holder.sectionTXT.visibility=View.VISIBLE
            holder?.sectionTXT?.text = context.getString(R.string.earlier)
            earlyFlag=true

        }
        else
        {
            holder?.sectionTXT?.visibility=View.GONE
        }

       // holder?.nameTXT?.text = items.events!!.get(position).user.username.toString()
        if(items.events?.get(position)?.message!=null)
        {
            holder?.notiBodyTXT?.text = Html.fromHtml(items.events?.get(position)?.message)
        }
        else
        {
            holder?.notiBodyTXT?.text = "null"
        }
        holder?.dateTXT?.text = appConstants.millisToDate(items.events!!.get(position).timestamp.toLong())

        Glide.with(context).load(items.events!!.get(position).user.photo_uri.medium)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(18)).placeholder(R.drawable.no_image_ic))
                //.transition(GenericTransitionOptions.with(R.anim.dialog_in))
                .into(holder.userIMG)

    }



    override fun getItemCount(): Int
    {
        return items.events!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}

class NotificationViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val sectionTXT = view.sectionTXT
    val nameTXT = view.nameTXT
    val notiBodyTXT = view.notiBodyTXT
    val userIMG = view.userIMG
    val dateTXT = view.dateTXT
}