package com.jeeran.business.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeeran.business.R
import kotlinx.android.synthetic.main.notification_setting_item_row.view.*


class NotificationSettingAdapter(private var _items : com.jeeran.business.models.chartFilters.ChartFiltersModel, private var _callback:NotificationSettingAdapterCallback) :
        RecyclerView.Adapter<NotificationSettingViewHolder>()
{


    var  items : com.jeeran.business.models.chartFilters.ChartFiltersModel
    var callback: NotificationSettingAdapterCallback
    lateinit var  context: Context;

    init
    {
        items=_items
        callback = _callback

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationSettingViewHolder
    {
        context=parent.context
        return NotificationSettingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_setting_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: NotificationSettingViewHolder, position: Int)
    {

       holder.nameTXT.setText(items.filters!!.get(position).name)

        if(items.filters!!.get(position).isChecked==1)
        {
            holder.valueSwitch.isChecked=true
        }
        else
        {
            holder.valueSwitch.isChecked=false
        }


        holder.valueSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch8=1 } else { switch8=0 }
//            setSetting()

            callback.onRecyclItemChecked(R.id.valueSwitch,position,isChecked)
        }


    }




    override fun getItemCount(): Int
    {
        return items.filters!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}

 class NotificationSettingViewHolder (view: View) : RecyclerView.ViewHolder(view)
{
    val nameTXT = view.nameTXT
    val valueSwitch = view.valueSwitch


}

 interface NotificationSettingAdapterCallback
{
    fun onRecyclItemClicked(viewID: Int, position: Int)
    fun onRecyclItemChecked(viewID: Int, position: Int,isChecked:Boolean)
}