package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeeran.business.R
import com.jeeran.business.databinding.SnoozeActivityBinding
import com.jeeran.business.utils.AppController
import com.jeeran.business.views.adapters.SnoozeNotifAdapter
import com.jeeran.business.views.adapters.snoozeAdapterCallback

class SnoozenActivity : AppCompatActivity(), snoozeAdapterCallback {
    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecyclItemClicked(viewID: Int, position: Int)
    {
        finish()
    }

    lateinit  var binding: SnoozeActivityBinding

    lateinit var timer:CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding=DataBindingUtil.setContentView(this, R.layout.snooze_activity)

        fillSnoozeData();


    }

    fun fillSnoozeData()
    {
        var items: MutableList<String> = ArrayList()
        lateinit var adapter: SnoozeNotifAdapter

        for(i in 0..10)
        {  items.add("Snooze "+ i+1+" Hour")
        }
        adapter = SnoozeNotifAdapter(items,this)
        binding.snoozeiLV.adapter=adapter
        binding.snoozeiLV.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL ,false)


    }
}
