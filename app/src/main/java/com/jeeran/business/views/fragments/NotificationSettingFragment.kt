package com.jeeran.business.views.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.databinding.NotificationSettingFragmentBinding
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.adapters.NotificationSettingAdapter
import com.jeeran.business.views.adapters.NotificationSettingAdapterCallback
import org.json.JSONArray
import org.json.JSONObject


class NotificationSettingFragment : BaseFragment(), com.jeeran.business.networking.RequestHandler.ResponseHandler, NotificationSettingAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, position: Int) {

      return

    }

    override fun onRecyclItemChecked(viewID: Int, position: Int, isChecked: Boolean)
    {
        if(isChecked==true)
        {
            chartFiltersModel.filters!!.get(position).isChecked=1
            setSetting()
        }
        else
        {
            chartFiltersModel.filters!!.get(position).isChecked=0
            setSetting()
        }


    }


    override fun successRequest(response: JSONObject, apiName: String,requestType: String) {
        if(apiName.equals(com.jeeran.business.networking.URLClass.getNotificationsSetngURL,true)&& requestType.equals("post",true))
        {
            var resArr:JSONArray=response.getJSONObject("settings").getJSONArray("flags")
            for (i in 0..resArr.length()-1)
            {
                if(resArr.getInt(i).toString().equals("1",true))
                {
                    chartFiltersModel.filters!!.get(i).isChecked=1
                }
                else
                {
                    chartFiltersModel.filters!!.get(i).isChecked=0
                }

            }
            adapter.notifyDataSetChanged()

//            if(resArr.getInt(0).toString().equals("1",true))
//            {
//                binding.switch0.isChecked=true
//            }
//            if(resArr.getInt(1).toString().equals("1",true))
//            {
//                binding.switch1.isChecked=true
//            }
//            if(resArr.getInt(2).toString().equals("1",true))
//            {
//                binding.switch2.isChecked=true
//            }
//            if(resArr.getInt(3).toString().equals("1",true))
//            {
//                binding.switch3.isChecked=true
//            }
//            if(resArr.getInt(4).toString().equals("1",true))
//            {
//                binding.switch4.isChecked=true
//            }
//            if(resArr.getInt(5).toString().equals("1",true))
//            {
//                binding.switch5.isChecked=true
//            }
//            if(resArr.getInt(6).toString().equals("1",true))
//            {
//                binding.switch6.isChecked=true
//            }
//            if(resArr.getInt(7).toString().equals("1",true))
//            {
//                binding.switch7.isChecked=true
//            }
//            if(resArr.getInt(8).toString().equals("1",true))
//            {
//                binding.switch8.isChecked=true
//            }

        }

        if(apiName.equals(com.jeeran.business.networking.URLClass.getChartFiltersURL,true)&& requestType.equals("get",true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), com.jeeran.business.models.chartFilters.ChartFiltersModel::class.java)
            chartFiltersModel = modelObj as com.jeeran.business.models.chartFilters.ChartFiltersModel

            adapter = NotificationSettingAdapter(chartFiltersModel, this)
            binding.notiStngLV.adapter = adapter
            binding.notiStngLV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

            getSetting()

        }

        if(apiName.equals(com.jeeran.business.networking.URLClass.setNotificationsSetngURL,true)&& requestType.equals("put",true))
        {
            //Toast.makeText(activity!!, R.string.success, Toast.LENGTH_SHORT).show()

        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }

    lateinit var binding: NotificationSettingFragmentBinding

    lateinit var chartFiltersModel: com.jeeran.business.models.chartFilters.ChartFiltersModel
     lateinit var  adapter: NotificationSettingAdapter

    var switch0=0;var switch1=0;var switch2=0;
    var switch3=0;var switch4=0;var switch5=0;
    var switch6=0;var switch7=0;var switch8=0



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.notification_setting_fragment, container, false)
        binding.lifecycleOwner = this
        val view = binding.root

        setScreenTitle(getString(R.string.notifi_dr))



        getSettingsFilters()

//
//        binding.switch0.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch0=1 } else { switch0=0 }
//            setSetting()
//        }
//
//        binding.switch1.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch1=1 } else { switch1=0 }
//            setSetting()
//        }
//
//        binding.switch2.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch2=1 } else { switch2=0 }
//            setSetting()
//        }
//
//        binding.switch3.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch3=1 } else { switch3=0 }
//            setSetting()
//        }
//
//        binding.switch4.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch4=1 } else { switch4=0 }
//            setSetting()
//        }
//
//        binding.switch5.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch5=1 } else { switch5=0 }
//            setSetting()
//        }
//
//        binding.switch6.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch6=1 } else { switch6=0 }
//            setSetting()
//        }
//
//        binding.switch7.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch7=1 } else { switch7=0 }
//            setSetting()
//        }
//
//        binding.switch8.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) { switch8=1 } else { switch8=0 }
//            setSetting()
//        }



        return view
    }

    fun getSetting()
    {
        var body= JSONObject()

        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("post", body, com.jeeran.business.networking.URLClass.getNotificationsSetngURL,activity as Activity)
    }

    fun setSetting()
    {
        var body= JSONObject()
        var arr= JSONArray()
//
//        arr.put(0,switch0)
//        arr.put(1,switch1)
//        arr.put(2,switch2)
//        arr.put(3,switch3)
//        arr.put(4,switch4)
//        arr.put(5,switch5)
//        arr.put(6,switch6)
//        arr.put(7,switch7)
//        arr.put(8,switch8)

        for(i in 0..chartFiltersModel.filters!!.size-1)
        {
            if(chartFiltersModel.filters!!.get(i).isChecked==1) {
                arr.put(i,1)
            }
            else
            {
                arr.put(i,0)
            }
        }

        var arrObject= JSONObject()
        arrObject.putOpt("allow_types",arr)

        body.putOpt("user_id", sharedPrefs.getInstance().getStringPreference(activity!!, appConstants.userID_KEY))
        body.putOpt("settings",arrObject)

        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("put", body, com.jeeran.business.networking.URLClass.setNotificationsSetngURL,activity as Activity)
    }


    fun getSettingsFilters()
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.getChartFiltersURL,activity!!)

    }
}
