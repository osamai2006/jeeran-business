package com.jeeran.business.views.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jeeran.business.R
import com.jeeran.business.models.countries.CountriesModel
import com.jeeran.business.networking.RequestHandler
import com.jeeran.business.views.adapters.CountryAdapter
import com.jeeran.business.views.adapters.CountryAdapterCallback
import org.json.JSONObject
import android.app.Activity
import android.content.Intent
import com.jeeran.business.databinding.CountriesActivityBinding
import com.jeeran.business.utils.AppController


class CountriesActivity : AppCompatActivity(), RequestHandler.ResponseHandler, CountryAdapterCallback {
    override fun onRecyclItemClicked(viewID: Int, cityID: Int, cityName: String)
    {
        val resultIntent = Intent()
        resultIntent.putExtra("cityID", cityID)
        resultIntent.putExtra("cityName", cityName)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()

    }

    override fun onRecyclItemLongClicked(viewID: Int, position: Int) {
        return
    }

    override fun successRequest(response: JSONObject, apiName: String, requestType: String) {

        if(apiName.equals(com.jeeran.business.networking.URLClass.citiesURL,true))
        {
            val gson = Gson()
            val modelObj = gson.fromJson<Any>(response.toString(), CountriesModel::class.java)
            countriesModel = modelObj as CountriesModel


            var adapter = CountryAdapter(countriesModel,this)
            binding.countrieLV.adapter=adapter
            binding.countrieLV.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL ,false)

//            for (i in 0..binding.citySpin.adapter.getCount()-1)
//            {
//                if(businessModel.city.id==citiesModel.items!!.get(i).id)
//                {
//                    binding.citySpin.setSelection(i)
//                    break
//                }
//
//            }
        }
    }

    override fun failedRequest(msg: String, apiName: String) {
        return
    }


    lateinit var binding: CountriesActivityBinding
    lateinit var countriesModel: CountriesModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppController.getInstance().setCurrentActivity(this)
        binding= DataBindingUtil.setContentView(this, R.layout.countries_activity)

        getCountries()

    }

    fun getCountries()
    {
        com.jeeran.business.networking.RequestHandler.getInstance(this).requestAPI("get", null, com.jeeran.business.networking.URLClass.citiesURL,this)

    }
}
