package com.jeeran.business.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.jeeran.business.R

abstract class BaseActivity : AppCompatActivity() {

    companion object
    {
        lateinit var titleTXT:TextView
        lateinit var notiCountTXT:TextView
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //binding2= DataBindingUtil.setContentView(this,R.layout.main_activity)

    }

    fun hideNavigationBarVisibility(visibility: Boolean) {
        var navigationBarVisibility  = true
        if (visibility) {
            val decorView = window.decorView
            val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            decorView.systemUiVisibility = uiOptions
            navigationBarVisibility = false
        } else
            navigationBarVisibility = true
    }

     fun setScreenTitle(title: String) {
         titleTXT = findViewById(R.id.toolbar_title)
         titleTXT.text = title
     }

    fun setNotificationCount(count: String) {
        notiCountTXT = findViewById(R.id.notifCountTXT)
        notiCountTXT.text = count
    }

    fun replaceFragment(fragment: Fragment,addToStack:Boolean) {
        var tx: FragmentTransaction
        tx = supportFragmentManager.beginTransaction()
        tx.replace(R.id.main_container, fragment)
        if(addToStack==true)
        {
            tx.addToBackStack(null)
        }

        tx.commit()
    }

    
}
