package com.jeeran.business.FirebaseServices

import android.app.NotificationManager
import android.app.PendingIntent

import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.text.Html
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.jeeran.business.R
import com.jeeran.business.utils.appConstants
import com.jeeran.business.utils.sharedPrefs
import com.jeeran.business.views.activities.SplashActivity


class FirebaseMsgService: FirebaseMessagingService()
{
    override fun onNewToken(p0: String?)
    {
        super.onNewToken(p0)

        var REGISTRATION_COMPLETE :String= "registrationComplete";

        val registrationToken = FirebaseInstanceId.getInstance().token
        sharedPrefs.getInstance().setStringPreference(this, appConstants.mobileToken_KEY, registrationToken.toString())

        val registrationComplete = Intent(REGISTRATION_COMPLETE)
        registrationComplete.putExtra("token", registrationToken)
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage)
    {
        super.onMessageReceived(remoteMessage)


//        val intent = Intent(this, SplashActivity::class.java)
//        //intent.putExtra("type", messageData);
//        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
//
//        val notifyImage = BitmapFactory.decodeResource(resources, R.mipmap.icon)
//        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//        val notificationBuilder = NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.icon)
//                .setLargeIcon(notifyImage)
//                .setColor(Color.parseColor("#FFE74C3C"))
//                .setContentText(remoteMessage?.getNotification()!!.getBody())
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent)
//        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//
//            val channel = NotificationChannel("default", getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)
//            channel.description = remoteMessage?.getNotification()!!.getBody()
//            notificationManager.createNotificationChannel(channel)
//            notificationBuilder.setChannelId("default")
//
//        }
//
//        notificationManager.notify(0, notificationBuilder.build())

        showNotification(remoteMessage)


    }

    private fun showNotification(remoteMessage: RemoteMessage)
    {
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setContentText(Html.fromHtml(remoteMessage.notification?.body))
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.icon)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }
}