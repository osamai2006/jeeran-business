package com.jeeran.business.utils

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.icu.text.SimpleDateFormat
import android.net.Uri
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import org.json.JSONArray
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*

class appConstants {

    companion object
    {
        var isLoggedInKEY="isloggedInKEY"
        var mobileToken_KEY="mobileToken_KEY"
        var language_KEY="language_KEY"
        var userToken_KEY="userToken_KEY"
        var userID_KEY="userID_KEY"
        var userName_KEY="userName_KEY"
        var userPhoto_KEY="userPhoto_KEY"
        var userEmail_KEY="userEmail_KEY"

        var userToken=""
        var loginURL=""
        var registerURL=""
        var IMAGE_DIRECTORY_NAME=""
        var webviewURL=""
        var dahsboardAnimationFinished=false
        var getBusinessCalled=false
        var getDashboardCalled=false
        var current_language="en"

        var reviewID=0

        var selectedSubCatArr: JSONArray = JSONArray()
        var selectedFeaturArr: JSONArray = JSONArray()


         fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
            val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
            vectorDrawable!!.setBounds(0, 0, 0, 0)
            val bitmap =
                    Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)
        }

        fun millisToDate(milliSeconds:Long):String
        {
//            val formatter = SimpleDateFormat("yyyy/MM/dd")
////            var dateString=formatter.format(Date(milliSeconds.toLong()))
////            return dateString
//
//            val calendar = Calendar.getInstance()
//            calendar.timeInMillis = milliSeconds
//            return formatter.format(calendar.time)


            val date = Date(milliSeconds * 1000L) // *1000 is to convert seconds to milliseconds
            val sdf = SimpleDateFormat("yyyy/MM/dd") // the format of your date
            //sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"))

            return sdf.format(date)

        }


        fun getBitmap(file: Uri, cr: ContentResolver): Bitmap?{
            var bitmap: Bitmap ?= null
            try {
                val inputStream = cr.openInputStream(file)
                bitmap = BitmapFactory.decodeStream(inputStream)
                // close stream
                try
                {
                    inputStream.close()
                }
                catch (e: IOException)
                {
                    e.toString()
                }

            }
            catch (e: FileNotFoundException)
            {
                e.toString()
            }
            return bitmap
        }


    }


}