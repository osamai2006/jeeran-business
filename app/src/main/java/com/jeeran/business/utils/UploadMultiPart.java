package com.jeeran.business.utils;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;

import com.jeeran.business.networking.URLClass;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;


import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class UploadMultiPart
{
   public static Uri filePath;
   public static   Uri fileUri;
    Bitmap bitmap;
    static long totalSize = 0;
//    static String ImageNameFromServer = "";
//    static String baseUrlFromServer = "";
//    static String urlFromServer = "";
   public static String IMAGE_DIRECTORY_NAME="";
   static Activity activity=null;
   static String url ="";

    public static UploadeResponse uploadeResponse = null;

    public UploadMultiPart(Activity activity,String placeID,String userID)
    {
        this.activity=activity;
        this.url=URLClass.Companion.getMultiPartsURL()+placeID+"&user_id="+userID;
    }



    public static void showFileChooser()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select Your Image"), 111);

    }

    public static Uri getOutputMediaFileUri(int type)
    {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs())
            {
                return null;

            }

        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    public static  String getRealPathFromDocumentUri( Uri uri){
        String filePath = "";

        try {


            Pattern p = Pattern.compile("(\\d+)$");
            Matcher m = p.matcher(uri.toString());
            if (!m.find()) {
                return filePath;
            }
            String imgId = m.group();

            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = activity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{imgId}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        catch (Exception xx){}

        return filePath;
    }

    public static class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

           // loadingWheel.startSpinwheel(activity!!, false, false)
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile()
        {

            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });


                File sourceFile = new File(getRealPathFromDocumentUri(filePath));

                // Adding file data to http body
                entity.addPart("userfile", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
//                entity.addPart("website",new StringBody("http://api.burgerfirefly.com"));
//                entity.addPart("email",new StringBody("osamai2006@gmail.com"));

                totalSize = entity.getContentLength();
               // httppost.addHeader("Authorization","Bearer "+appConstants.userToken);
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                    responseString = responseString.replace("\"","");

                }
                else
                {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            try
            {
                //appConstants.stopSpinWheel();
//                JSONObject jsonObjResp = new JSONObject(result.toString());
//                final String status=jsonObjResp.getString("success");
//
//                if(status.equalsIgnoreCase("true"))
//                {
//                    JSONObject mainObj=jsonObjResp.getJSONObject("data");
////                    ImageNameFromServer =mainObj.getString("path");
////                    baseUrlFromServer =mainObj.getString("baseUrl");
////                    urlFromServer =mainObj.getString("url");
//                }
//                else
//                {
//
//                }


            }
            catch (Exception e) {
                e.printStackTrace();
            }


            super.onPostExecute(result);
            uploadeResponse.uploadFinish(result);
        }


    }

    public interface UploadeResponse {
        void uploadFinish(String result);
    }
}
