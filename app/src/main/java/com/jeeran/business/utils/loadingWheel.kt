package com.jeeran.business.utils

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.SystemClock
import android.widget.ProgressBar
import com.jeeran.business.R

class loadingWheel
{


    companion object
    {
        var spinWheelDialog: Dialog? = null
        public fun startSpinwheel(context: Context, setDefaultLifetime: Boolean, isCancelable: Boolean) {
            try {

                if (spinWheelDialog != null && spinWheelDialog!!.isShowing)
                    return

                spinWheelDialog = Dialog(context, R.style.wait_spinner_style)
                val progressBar = ProgressBar(context)
                val layoutParams = ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT)
                spinWheelDialog!!.addContentView(progressBar, layoutParams)
                spinWheelDialog!!.setCancelable(isCancelable)
                spinWheelDialog!!.show()

                val spinWheelTimer = Handler()
                spinWheelTimer.removeCallbacks(dismissSpinner)

                if (setDefaultLifetime)
                // If requested for default dismiss time.
                    spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + 1000)

                spinWheelDialog!!.setCanceledOnTouchOutside(false)
            } catch (xx: Exception) {
                xx.message
            }

        }

        internal var dismissSpinner: Runnable = Runnable { stopLoading() }

        fun stopLoading() {
            if (spinWheelDialog != null)
                try {
                    spinWheelDialog!!.dismiss()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                }

            spinWheelDialog = null
        }
    }


}