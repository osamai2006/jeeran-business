package com.jeeran.business.utils

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import java.lang.Exception

class AppController: Application()
{
    val CHANNEL_ID = "default"
    val CHANNEL_NAME = "default"

    companion object
    {

        private  lateinit var instance: AppController
        private lateinit var currentActivity: Activity

        fun getInstance(): AppController {

                instance = AppController()

            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        createNotificationChannel()
    }

    fun getCurrentActivity(): Activity {

        return currentActivity
    }

    fun setCurrentActivity(_currentActivity: Activity) {
        currentActivity = _currentActivity
    }

    private fun createNotificationChannel()
    {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val serviceChannel = NotificationChannel(
                        CHANNEL_ID,
                        CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_HIGH
                )

                val manager = getSystemService(NotificationManager::class.java)
                serviceChannel.setSound(null, null)
                manager.createNotificationChannel(serviceChannel)
            }
        }
        catch (xx:Exception)
        {
            xx.toString()
        }
    }




}